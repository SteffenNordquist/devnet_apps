﻿namespace Invoice
{
    partial class ViewUpdateInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgviewinvoice = new System.Windows.Forms.DataGridView();
            this.invoicenum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clientidno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clientname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.street = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.city = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datenow = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datedue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalamt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnEdit = new System.Windows.Forms.Button();
            this.dt2 = new System.Windows.Forms.DateTimePicker();
            this.lblvat = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.vatno = new System.Windows.Forms.RadioButton();
            this.vatyes = new System.Windows.Forms.RadioButton();
            this.txtdescription = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtclientacctno = new System.Windows.Forms.TextBox();
            this.txttotalamt = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.txtclientname = new System.Windows.Forms.TextBox();
            this.btnSaveInvoice = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtstreet = new System.Windows.Forms.TextBox();
            this.txtamount = new System.Windows.Forms.TextBox();
            this.txtcountry = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtclientrep = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtcity = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtdatedue = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtdatenow = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtInvoicenum = new System.Windows.Forms.TextBox();
            this.txtnote = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxclients = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgviewinvoice)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgviewinvoice
            // 
            this.dgviewinvoice.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dgviewinvoice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgviewinvoice.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.invoicenum,
            this.clientidno,
            this.clientname,
            this.street,
            this.city,
            this.country,
            this.rep,
            this.desc,
            this.note,
            this.datenow,
            this.datedue,
            this.amt,
            this.vat,
            this.totalamt});
            this.dgviewinvoice.Location = new System.Drawing.Point(14, 14);
            this.dgviewinvoice.Name = "dgviewinvoice";
            this.dgviewinvoice.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgviewinvoice.Size = new System.Drawing.Size(444, 534);
            this.dgviewinvoice.TabIndex = 0;
            this.dgviewinvoice.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgviewinvoice_CellContentClick);
            // 
            // invoicenum
            // 
            this.invoicenum.HeaderText = "Invoice ID";
            this.invoicenum.Name = "invoicenum";
            // 
            // clientidno
            // 
            this.clientidno.HeaderText = "Client ID";
            this.clientidno.Name = "clientidno";
            // 
            // clientname
            // 
            this.clientname.HeaderText = "Client Name";
            this.clientname.Name = "clientname";
            // 
            // street
            // 
            this.street.HeaderText = "Street";
            this.street.Name = "street";
            // 
            // city
            // 
            this.city.HeaderText = "City";
            this.city.Name = "city";
            // 
            // country
            // 
            this.country.HeaderText = "Country";
            this.country.Name = "country";
            // 
            // rep
            // 
            this.rep.HeaderText = "Representative";
            this.rep.Name = "rep";
            // 
            // desc
            // 
            this.desc.HeaderText = "Description";
            this.desc.Name = "desc";
            // 
            // note
            // 
            this.note.HeaderText = "Note";
            this.note.Name = "note";
            // 
            // datenow
            // 
            this.datenow.HeaderText = "Date";
            this.datenow.Name = "datenow";
            // 
            // datedue
            // 
            this.datedue.HeaderText = "Date Due";
            this.datedue.Name = "datedue";
            // 
            // amt
            // 
            this.amt.HeaderText = "Amount";
            this.amt.Name = "amt";
            // 
            // vat
            // 
            this.vat.HeaderText = "VAT";
            this.vat.Name = "vat";
            // 
            // totalamt
            // 
            this.totalamt.HeaderText = "Total Amount";
            this.totalamt.Name = "totalamt";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox1.Controls.Add(this.btnEdit);
            this.groupBox1.Controls.Add(this.dt2);
            this.groupBox1.Controls.Add(this.lblvat);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.vatno);
            this.groupBox1.Controls.Add(this.vatyes);
            this.groupBox1.Controls.Add(this.txtdescription);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtclientacctno);
            this.groupBox1.Controls.Add(this.txttotalamt);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.btnGenerate);
            this.groupBox1.Controls.Add(this.txtclientname);
            this.groupBox1.Controls.Add(this.btnSaveInvoice);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtstreet);
            this.groupBox1.Controls.Add(this.txtamount);
            this.groupBox1.Controls.Add(this.txtcountry);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtclientrep);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtcity);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtdatedue);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtdatenow);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtInvoicenum);
            this.groupBox1.Controls.Add(this.txtnote);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.comboBoxclients);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(471, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(780, 534);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            // 
            // btnEdit
            // 
            this.btnEdit.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Location = new System.Drawing.Point(595, 379);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(148, 36);
            this.btnEdit.TabIndex = 39;
            this.btnEdit.Text = "EDIT";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // dt2
            // 
            this.dt2.Enabled = false;
            this.dt2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dt2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt2.Location = new System.Drawing.Point(741, 112);
            this.dt2.Name = "dt2";
            this.dt2.Size = new System.Drawing.Size(20, 26);
            this.dt2.TabIndex = 38;
            this.dt2.ValueChanged += new System.EventHandler(this.dt2_ValueChanged);
            // 
            // lblvat
            // 
            this.lblvat.AutoSize = true;
            this.lblvat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblvat.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblvat.Location = new System.Drawing.Point(569, 291);
            this.lblvat.Name = "lblvat";
            this.lblvat.Size = new System.Drawing.Size(2, 25);
            this.lblvat.TabIndex = 37;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(416, 293);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(87, 19);
            this.label16.TabIndex = 36;
            this.label16.Text = "VAT Added:";
            // 
            // vatno
            // 
            this.vatno.AutoSize = true;
            this.vatno.Location = new System.Drawing.Point(691, 239);
            this.vatno.Name = "vatno";
            this.vatno.Size = new System.Drawing.Size(54, 27);
            this.vatno.TabIndex = 34;
            this.vatno.TabStop = true;
            this.vatno.Text = "NO";
            this.vatno.UseVisualStyleBackColor = true;
            this.vatno.CheckedChanged += new System.EventHandler(this.vatno_CheckedChanged);
            // 
            // vatyes
            // 
            this.vatyes.AutoSize = true;
            this.vatyes.Location = new System.Drawing.Point(568, 239);
            this.vatyes.Name = "vatyes";
            this.vatyes.Size = new System.Drawing.Size(56, 27);
            this.vatyes.TabIndex = 33;
            this.vatyes.TabStop = true;
            this.vatyes.Text = "YES";
            this.vatyes.UseVisualStyleBackColor = true;
            this.vatyes.CheckedChanged += new System.EventHandler(this.vatyes_CheckedChanged);
            // 
            // txtdescription
            // 
            this.txtdescription.Enabled = false;
            this.txtdescription.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdescription.Location = new System.Drawing.Point(24, 419);
            this.txtdescription.Multiline = true;
            this.txtdescription.Name = "txtdescription";
            this.txtdescription.Size = new System.Drawing.Size(224, 94);
            this.txtdescription.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(416, 348);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 19);
            this.label11.TabIndex = 25;
            this.label11.Text = "TOTAL:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(101, 269);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 19);
            this.label15.TabIndex = 32;
            this.label15.Text = "Country:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(416, 249);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 19);
            this.label10.TabIndex = 24;
            this.label10.Text = "Include VAT?";
            // 
            // txtclientacctno
            // 
            this.txtclientacctno.Enabled = false;
            this.txtclientacctno.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtclientacctno.Location = new System.Drawing.Point(185, 61);
            this.txtclientacctno.Name = "txtclientacctno";
            this.txtclientacctno.Size = new System.Drawing.Size(216, 27);
            this.txtclientacctno.TabIndex = 7;
            // 
            // txttotalamt
            // 
            this.txttotalamt.Enabled = false;
            this.txttotalamt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotalamt.Location = new System.Drawing.Point(568, 337);
            this.txttotalamt.Name = "txttotalamt";
            this.txttotalamt.Size = new System.Drawing.Size(194, 27);
            this.txttotalamt.TabIndex = 23;
            this.txttotalamt.TextChanged += new System.EventHandler(this.txttotalamt_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(101, 225);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 19);
            this.label14.TabIndex = 31;
            this.label14.Text = "City:";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Enabled = false;
            this.btnGenerate.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerate.Location = new System.Drawing.Point(595, 463);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(150, 36);
            this.btnGenerate.TabIndex = 21;
            this.btnGenerate.Text = "GENERATE PDF";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // txtclientname
            // 
            this.txtclientname.Enabled = false;
            this.txtclientname.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtclientname.Location = new System.Drawing.Point(185, 105);
            this.txtclientname.Name = "txtclientname";
            this.txtclientname.Size = new System.Drawing.Size(216, 27);
            this.txtclientname.TabIndex = 8;
            // 
            // btnSaveInvoice
            // 
            this.btnSaveInvoice.Enabled = false;
            this.btnSaveInvoice.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveInvoice.Location = new System.Drawing.Point(595, 421);
            this.btnSaveInvoice.Name = "btnSaveInvoice";
            this.btnSaveInvoice.Size = new System.Drawing.Size(148, 36);
            this.btnSaveInvoice.TabIndex = 19;
            this.btnSaveInvoice.Text = "SAVE INVOICE";
            this.btnSaveInvoice.UseVisualStyleBackColor = true;
            this.btnSaveInvoice.Click += new System.EventHandler(this.btnSaveInvoice_Click_1);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(101, 181);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 19);
            this.label13.TabIndex = 30;
            this.label13.Text = "Street:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(416, 211);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(111, 19);
            this.label9.TabIndex = 18;
            this.label9.Text = "Amount (PHP):";
            // 
            // txtstreet
            // 
            this.txtstreet.Enabled = false;
            this.txtstreet.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstreet.Location = new System.Drawing.Point(185, 170);
            this.txtstreet.Name = "txtstreet";
            this.txtstreet.Size = new System.Drawing.Size(216, 27);
            this.txtstreet.TabIndex = 9;
            // 
            // txtamount
            // 
            this.txtamount.Enabled = false;
            this.txtamount.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtamount.Location = new System.Drawing.Point(568, 201);
            this.txtamount.Name = "txtamount";
            this.txtamount.Size = new System.Drawing.Size(194, 27);
            this.txtamount.TabIndex = 17;
            this.txtamount.TextChanged += new System.EventHandler(this.txtamount_TextChanged);
            // 
            // txtcountry
            // 
            this.txtcountry.Enabled = false;
            this.txtcountry.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcountry.Location = new System.Drawing.Point(185, 257);
            this.txtcountry.Name = "txtcountry";
            this.txtcountry.Size = new System.Drawing.Size(216, 27);
            this.txtcountry.TabIndex = 29;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(418, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 19);
            this.label3.TabIndex = 5;
            this.label3.Text = "Date Due:";
            // 
            // txtclientrep
            // 
            this.txtclientrep.Enabled = false;
            this.txtclientrep.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtclientrep.Location = new System.Drawing.Point(185, 324);
            this.txtclientrep.Name = "txtclientrep";
            this.txtclientrep.Size = new System.Drawing.Size(216, 27);
            this.txtclientrep.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(416, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 19);
            this.label2.TabIndex = 4;
            this.label2.Text = "Date:";
            // 
            // txtcity
            // 
            this.txtcity.Enabled = false;
            this.txtcity.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcity.Location = new System.Drawing.Point(185, 213);
            this.txtcity.Name = "txtcity";
            this.txtcity.Size = new System.Drawing.Size(216, 27);
            this.txtcity.TabIndex = 28;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(416, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "Invoice Number:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 19);
            this.label4.TabIndex = 11;
            this.label4.Text = "Client Account No:";
            // 
            // txtdatedue
            // 
            this.txtdatedue.Enabled = false;
            this.txtdatedue.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdatedue.Location = new System.Drawing.Point(568, 157);
            this.txtdatedue.Name = "txtdatedue";
            this.txtdatedue.Size = new System.Drawing.Size(194, 27);
            this.txtdatedue.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(257, 392);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(120, 19);
            this.label12.TabIndex = 27;
            this.label12.Text = "Note (Optional):";
            // 
            // txtdatenow
            // 
            this.txtdatenow.Enabled = false;
            this.txtdatenow.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdatenow.Location = new System.Drawing.Point(568, 112);
            this.txtdatenow.Name = "txtdatenow";
            this.txtdatenow.Size = new System.Drawing.Size(167, 27);
            this.txtdatenow.TabIndex = 1;
            this.txtdatenow.TextChanged += new System.EventHandler(this.txtdatenow_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 19);
            this.label5.TabIndex = 12;
            this.label5.Text = "Client Name:";
            // 
            // txtInvoicenum
            // 
            this.txtInvoicenum.Enabled = false;
            this.txtInvoicenum.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInvoicenum.Location = new System.Drawing.Point(567, 61);
            this.txtInvoicenum.Name = "txtInvoicenum";
            this.txtInvoicenum.Size = new System.Drawing.Size(194, 27);
            this.txtInvoicenum.TabIndex = 0;
            // 
            // txtnote
            // 
            this.txtnote.Enabled = false;
            this.txtnote.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnote.Location = new System.Drawing.Point(261, 419);
            this.txtnote.Multiline = true;
            this.txtnote.Name = "txtnote";
            this.txtnote.Size = new System.Drawing.Size(207, 94);
            this.txtnote.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(20, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 19);
            this.label6.TabIndex = 13;
            this.label6.Text = "Address:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(20, 335);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 19);
            this.label7.TabIndex = 14;
            this.label7.Text = "Representative:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(20, 392);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 19);
            this.label8.TabIndex = 16;
            this.label8.Text = "Description:";
            // 
            // comboBoxclients
            // 
            this.comboBoxclients.Enabled = false;
            this.comboBoxclients.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxclients.FormattingEnabled = true;
            this.comboBoxclients.Location = new System.Drawing.Point(20, 23);
            this.comboBoxclients.Name = "comboBoxclients";
            this.comboBoxclients.Size = new System.Drawing.Size(290, 27);
            this.comboBoxclients.TabIndex = 20;
            this.comboBoxclients.Text = "Choose Client:";
            this.comboBoxclients.SelectedIndexChanged += new System.EventHandler(this.comboBoxclients_SelectedIndexChanged);
            // 
            // ViewUpdateInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(1266, 563);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgviewinvoice);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ViewUpdateInvoice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ViewUpdateInvoice";
            this.Load += new System.EventHandler(this.ViewUpdateInvoice_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgviewinvoice)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgviewinvoice;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dt2;
        private System.Windows.Forms.Label lblvat;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.RadioButton vatno;
        private System.Windows.Forms.RadioButton vatyes;
        private System.Windows.Forms.TextBox txtdescription;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtclientacctno;
        private System.Windows.Forms.TextBox txttotalamt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.TextBox txtclientname;
        private System.Windows.Forms.Button btnSaveInvoice;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtstreet;
        private System.Windows.Forms.TextBox txtamount;
        private System.Windows.Forms.TextBox txtcountry;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtclientrep;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtcity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtdatedue;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtdatenow;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtInvoicenum;
        private System.Windows.Forms.TextBox txtnote;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxclients;
        private System.Windows.Forms.DataGridViewTextBoxColumn invoicenum;
        private System.Windows.Forms.DataGridViewTextBoxColumn clientidno;
        private System.Windows.Forms.DataGridViewTextBoxColumn clientname;
        private System.Windows.Forms.DataGridViewTextBoxColumn street;
        private System.Windows.Forms.DataGridViewTextBoxColumn city;
        private System.Windows.Forms.DataGridViewTextBoxColumn country;
        private System.Windows.Forms.DataGridViewTextBoxColumn rep;
        private System.Windows.Forms.DataGridViewTextBoxColumn desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn note;
        private System.Windows.Forms.DataGridViewTextBoxColumn datenow;
        private System.Windows.Forms.DataGridViewTextBoxColumn datedue;
        private System.Windows.Forms.DataGridViewTextBoxColumn amt;
        private System.Windows.Forms.DataGridViewTextBoxColumn vat;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalamt;
        private System.Windows.Forms.Button btnEdit;
    }
}