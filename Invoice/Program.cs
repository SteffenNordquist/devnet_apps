﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Invoice
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new CreateInvoice());
            //Application.Run(new ViewUpdateClient());
            //Application.Run(new ViewUpdateInvoice());
            Application.Run(new InvoiceMDI());
            //Application.Run(new AddClient());
            //Application.Run(new AddEmployee());
            //Application.Run(new ViewUpdateEmployeeInfo());

        }
    }
}
