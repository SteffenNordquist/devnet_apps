﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Invoice
{
    public partial class AddEmployee : Form
    {
        public AddEmployee()
        {
            InitializeComponent();
        }

        public static MongoCollection<EmployeeEntity> Employees = new MongoClient(@"mongodb://client144:client144devnetworks@144.76.166.207/DevNet").GetServer().GetDatabase("DevNet").GetCollection<EmployeeEntity>("EmployeeInfo");

        private void EmployeeInfo_Load(object sender, EventArgs e)
        {
            int employeeid = Convert.ToInt32(Employees.Count());
            string text = employeeid.ToString("0");
            int text2 = Convert.ToInt16(text);
            text2++;
            string text3 = Convert.ToString(text2);
            if (text3.Length <= 1)
            {
                txtempid.Text = ("-" + "00" + text3);
            }
            else if (text3.Length <= 2)
            {
                txtempid.Text = ("-" + "0" + text3);
            }
            

            var months = System.Globalization.DateTimeFormatInfo.InvariantInfo.MonthNames;
            cmbempbirthmonth.DataSource = months;

            var day = Enumerable.Range(1, 30).ToList();
            cmbempbirthday.DataSource = day;

            var year = Enumerable.Range(1950, DateTime.Now.Year).ToList();
            cmbempbirthyear.DataSource = year;
            
        }

        private void txtemplastname_Click(object sender, EventArgs e)
        {
            txtemplastname.Text = "";
        }

        private void txtempfirstname_Click(object sender, EventArgs e)
        {
            txtempfirstname.Text = "";
        }

        private void txtempmidname_Click(object sender, EventArgs e)
        {
            txtempmidname.Text = "";
        }

        private void dtstartdate_ValueChanged(object sender, EventArgs e)
        {
           
            
        }
        
        private void btnSaveEmpInfo_Click(object sender, EventArgs e)
        {
            if (txtemplastname.Text != "" &&
                txtempfirstname.Text != "" &&
                txtempmidname.Text != "" &&
                txthomeaddress.Text != "" &&
                txtcontactno.Text != "" &&
                txtemailaddress.Text != "" &&
                txtskypename.Text != "" &&
                txtfbacct.Text != "" &&
                txtssno.Text != "" &&
                txtphno.Text != "" &&
                txttin.Text != "" &&
                txtpagibigno.Text != "" &&
                txtgender.Text != "" &&
                txtstatus.Text != "" &&
                txtethnicity.Text != "" &&
                txtposition.Text != "" &&
                txtstartdate.Text != "" &&
                txtworklocation.Text != "" &&
                txtphone.Text != "" &&
                txtcontactname.Text != "" &&
                txtcontactaddress.Text != "" &&
                txtpriphone.Text != "" &&
                txtalterphone.Text != "" &&
                txtrelationship.Text != ""
                )
            {
                EmployeeEntity employee = new EmployeeEntity
                   {
                       empid = txtstartdatesuffix.Text + txtempid.Text,
                       emplastname = txtemplastname.Text,
                       empfirstname = txtempfirstname.Text,
                       empmiddlename = txtempmidname.Text,
                       empbirthmonth = cmbempbirthmonth.Text,
                       empbirthday = Convert.ToInt32(cmbempbirthday.Text),
                       empbirthyear = Convert.ToInt32(cmbempbirthyear.Text),
                       emphomeaddress = txthomeaddress.Text,
                       empcontactno = txtcontactno.Text,
                       empemailaddress = txtemailaddress.Text,
                       empskypename = txtskypename.Text,
                       empfbacct = txtfbacct.Text,
                       empSSSNo = txtssno.Text,
                       empPhilHealthNo = txtphno.Text,
                       empTIN = txttin.Text,
                       empPagIbigNo = txtpagibigno.Text,
                       empgender = txtgender.Text,
                       empstatus = txtstatus.Text,
                       empethnicity = txtethnicity.Text,
                       empposition = txtposition.Text,
                       empstartdate = Convert.ToDateTime(txtstartdate.Text),
                       empworklocation = txtworklocation.Text,
                       empworkphone = txtphone.Text,
                       contactname = txtcontactname.Text,
                       contactaddress = txtcontactaddress.Text,
                       primaryphone = txtpriphone.Text,
                       alterphone = txtalterphone.Text,
                       relationship = txtrelationship.Text
                   };

                Employees.Insert(employee);
                MessageBox.Show("Successfully Saved!");
                btnSaveEmpInfo.Enabled = false;
            }
            else
            {
                MessageBox.Show("Please fill-in necessary details!");
            } 
        }

        private void cmbempbirthmonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedmonth = cmbempbirthmonth.Text;
        }

        private void cmbempbirthday_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedday = cmbempbirthday.Text;
        }

        private void cmbempbirthyear_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedyear = cmbempbirthyear.Text;
        }

        private void rbmale_CheckedChanged(object sender, EventArgs e)
        {
            if (rbmale.Checked)
            { txtgender.Text = rbmale.Text; }
            else if (rbfemale.Checked)
            { txtgender.Text = rbfemale.Text; }
        }

        private void rbfemale_CheckedChanged(object sender, EventArgs e)
        {
            if (rbfemale.Checked)
            { txtgender.Text = rbfemale.Text; }
            else if (rbmale.Checked)
            { txtgender.Text = rbmale.Text; }
        }

        private void rbsingle_CheckedChanged(object sender, EventArgs e)
        {
            if (rbsingle.Checked)
            { txtstatus.Text = rbsingle.Text; }
            else if (rbmarried.Checked)
            { txtstatus.Text = rbmarried.Text; }
            else if (rbseparated.Checked)
            { txtstatus.Text = rbseparated.Text; }
            else if (rbdivorced.Checked)
            { txtstatus.Text = rbdivorced.Text; }
        }

        private void rbdivorced_CheckedChanged(object sender, EventArgs e)
        {
            if (rbdivorced.Checked)
            { txtstatus.Text = rbdivorced.Text; }
            else if (rbmarried.Checked)
            { txtstatus.Text = rbmarried.Text; }
            else if (rbseparated.Checked)
            { txtstatus.Text = rbseparated.Text; }
            else if (rbsingle.Checked)
            { txtstatus.Text = rbsingle.Text; }
        }

        private void rbmarried_CheckedChanged(object sender, EventArgs e)
        {
            if (rbmarried.Checked)
            { txtstatus.Text = rbmarried.Text; }
            else if (rbsingle.Checked)
            { txtstatus.Text = rbsingle.Text; }
            else if (rbseparated.Checked)
            { txtstatus.Text = rbseparated.Text; }
            else if (rbdivorced.Checked)
            { txtstatus.Text = rbdivorced.Text; }
        }

        private void rbseparated_CheckedChanged(object sender, EventArgs e)
        {
            if (rbseparated.Checked)
            { txtstatus.Text = rbseparated.Text; }
            else if (rbmarried.Checked)
            { txtstatus.Text = rbmarried.Text; }
            else if (rbsingle.Checked)
            { txtstatus.Text = rbsingle.Text; }
            else if (rbdivorced.Checked)
            { txtstatus.Text = rbdivorced.Text; }
        }

        private void rbafrican_CheckedChanged(object sender, EventArgs e)
        {
            if (rbafrican.Checked)
            { txtethnicity.Text = rbafrican.Text; }
            else if (rbasian.Checked)
            { txtethnicity.Text = rbasian.Text; }
            else if (rbhispanic.Checked)
            { txtethnicity.Text = rbhispanic.Text; }
            else if (rbwhite.Checked)
            { txtethnicity.Text = rbwhite.Text; }
            else if (rbalaskan.Checked)
            { txtethnicity.Text = rbalaskan.Text; }
        }

        private void rbasian_CheckedChanged(object sender, EventArgs e)
        {
            if (rbasian.Checked)
            { txtethnicity.Text = rbasian.Text; }
            else if (rbafrican.Checked)
            { txtethnicity.Text = rbafrican.Text; }
            else if (rbhispanic.Checked)
            { txtethnicity.Text = rbhispanic.Text; }
            else if (rbwhite.Checked)
            { txtethnicity.Text = rbwhite.Text; }
            else if (rbalaskan.Checked)
            { txtethnicity.Text = rbalaskan.Text; }
        }

        private void rbhispanic_CheckedChanged(object sender, EventArgs e)
        {
            if (rbhispanic.Checked)
            { txtethnicity.Text = rbhispanic.Text; }
            else if (rbasian.Checked)
            { txtethnicity.Text = rbasian.Text; }
            else if (rbafrican.Checked)
            { txtethnicity.Text = rbafrican.Text; }
            else if (rbwhite.Checked)
            { txtethnicity.Text = rbwhite.Text; }
            else if (rbalaskan.Checked)
            { txtethnicity.Text = rbalaskan.Text; }
        }

        private void rbwhite_CheckedChanged(object sender, EventArgs e)
        {
            if (rbwhite.Checked)
            { txtethnicity.Text = rbwhite.Text; }
            else if (rbasian.Checked)
            { txtethnicity.Text = rbasian.Text; }
            else if (rbhispanic.Checked)
            { txtethnicity.Text = rbhispanic.Text; }
            else if (rbafrican.Checked)
            { txtethnicity.Text = rbafrican.Text; }
            else if (rbalaskan.Checked)
            { txtethnicity.Text = rbalaskan.Text; }
        }

        private void rbalaskan_CheckedChanged(object sender, EventArgs e)
        {
            if (rbalaskan.Checked)
            { txtethnicity.Text = rbalaskan.Text; }
            else if (rbasian.Checked)
            { txtethnicity.Text = rbasian.Text; }
            else if (rbhispanic.Checked)
            { txtethnicity.Text = rbhispanic.Text; }
            else if (rbwhite.Checked)
            { txtethnicity.Text = rbwhite.Text; }
            else if (rbafrican.Checked)
            { txtethnicity.Text = rbafrican.Text; }
        }

        

        private void dtstartdate_ValueChanged_1(object sender, EventArgs e)
        {
            txtstartdate.Text = dtstartdate.Value.ToShortDateString();
        }

        private void txtempid_TextChanged(object sender, EventArgs e)
        {

        }

        
        
    }
}
