﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Invoice
{
    public partial class ViewUpdateClient : Form
    {
        public ViewUpdateClient()
        {
            InitializeComponent();
        }

        public static MongoCollection<ClientEntity> Client = new MongoClient(@"mongodb://client144:client144devnetworks@144.76.166.207/DevNet").GetServer().GetDatabase("DevNet").GetCollection<ClientEntity>("Clients");  

        private void ViewUpdateClient_Load(object sender, EventArgs e)
        {
            var client = Client.FindAll();

            foreach (var showclients in client)
            {
                dgviewclient.Rows.Add(showclients.clientId,
                                      showclients.clientAcctName,
                                      showclients.clientStreet,
                                      showclients.clientCity,
                                      showclients.clientCountry,
                                      showclients.clientRep);
            }
        }

        private void dgviewclient_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            txtacctname.Enabled = true;
            txtstreet.Enabled = true;
            txtcity.Enabled = true;
            txtcountry.Enabled = true;
            txtrep.Enabled = true;
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            
            //var query = Client.FindOne(Query.EQ("_id", txtclientid.Text));

            UpdateBuilder update = MongoDB.Driver.Builders.Update.Set("clientAcctName", txtacctname.Text)
                                                .Set("clientStreet", txtstreet.Text)
                                                .Set("clientCity", txtcity.Text)
                                                .Set("clientCountry", txtcountry.Text)
                                                .Set("clientRep", txtrep.Text);

            Client.Update(Query.EQ("_id", txtclientid.Text), update);
            MessageBox.Show("Successfully Updated!", "Update Client");
        }

        private void dgviewclient_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dgviewclient.Rows[e.RowIndex];

                txtclientid.Text = row.Cells["id"].Value.ToString();
                txtacctname.Text = row.Cells["name"].Value.ToString();
                txtstreet.Text = row.Cells["street"].Value.ToString();
                txtcity.Text = row.Cells["city"].Value.ToString();
                txtcountry.Text = row.Cells["country"].Value.ToString();
                txtrep.Text = row.Cells["rep"].Value.ToString();

            }
        }

        private void txtclientid_TextChanged(object sender, EventArgs e)
        {
            //txtclientid.Text = txtAcctName.Text.ToUpper();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
