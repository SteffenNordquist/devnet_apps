﻿namespace Invoice
{
    partial class ViewUpdateEmployeeInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgEmployeeInfo = new System.Windows.Forms.DataGridView();
            this.empid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emplastname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empfirstname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empmidname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empmonth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empyear = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emphomeaddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empcontactno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empemailadd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empskypename = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empfbacct = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empsssno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empphno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emptin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emppagibigno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empgender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empstatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empethnicity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.position = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empstartdate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empworklocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empworkphone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contactname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contactaddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priphone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alterphone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.relationship = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSaveEmpInfo = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtrelationship = new System.Windows.Forms.TextBox();
            this.txtalterphone = new System.Windows.Forms.TextBox();
            this.txtpriphone = new System.Windows.Forms.TextBox();
            this.txtcontactaddress = new System.Windows.Forms.TextBox();
            this.txtcontactname = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtstartdate = new System.Windows.Forms.TextBox();
            this.txtworklocation = new System.Windows.Forms.TextBox();
            this.txtphone = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtposition = new System.Windows.Forms.TextBox();
            this.txtempid = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtyear = new System.Windows.Forms.TextBox();
            this.cmbempbirthday = new System.Windows.Forms.ComboBox();
            this.cmbempbirthyear = new System.Windows.Forms.ComboBox();
            this.cmbempbirthmonth = new System.Windows.Forms.ComboBox();
            this.txtmonth = new System.Windows.Forms.TextBox();
            this.txtday = new System.Windows.Forms.TextBox();
            this.txtpagibigno = new System.Windows.Forms.TextBox();
            this.txtphno = new System.Windows.Forms.TextBox();
            this.txttin = new System.Windows.Forms.TextBox();
            this.txtssno = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtfbacct = new System.Windows.Forms.TextBox();
            this.txtskypename = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtemailaddress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txthomeaddress = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtcontactno = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtempmidname = new System.Windows.Forms.TextBox();
            this.txtemplastname = new System.Windows.Forms.TextBox();
            this.txtempfirstname = new System.Windows.Forms.TextBox();
            this.txtgender = new System.Windows.Forms.TextBox();
            this.txtstatus = new System.Windows.Forms.TextBox();
            this.txtethnicity = new System.Windows.Forms.TextBox();
            this.btnEditEmployeeInfo = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.rbafrican = new System.Windows.Forms.RadioButton();
            this.rbasian = new System.Windows.Forms.RadioButton();
            this.label26 = new System.Windows.Forms.Label();
            this.rbhispanic = new System.Windows.Forms.RadioButton();
            this.rbalaskan = new System.Windows.Forms.RadioButton();
            this.rbwhite = new System.Windows.Forms.RadioButton();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rbseparated = new System.Windows.Forms.RadioButton();
            this.label28 = new System.Windows.Forms.Label();
            this.rbdivorced = new System.Windows.Forms.RadioButton();
            this.rbsingle = new System.Windows.Forms.RadioButton();
            this.rbmarried = new System.Windows.Forms.RadioButton();
            this.label29 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbfemale = new System.Windows.Forms.RadioButton();
            this.rbmale = new System.Windows.Forms.RadioButton();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgEmployeeInfo)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgEmployeeInfo
            // 
            this.dgEmployeeInfo.AllowUserToAddRows = false;
            this.dgEmployeeInfo.AllowUserToDeleteRows = false;
            this.dgEmployeeInfo.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dgEmployeeInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEmployeeInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.empid,
            this.emplastname,
            this.empfirstname,
            this.empmidname,
            this.empmonth,
            this.empday,
            this.empyear,
            this.emphomeaddress,
            this.empcontactno,
            this.empemailadd,
            this.empskypename,
            this.empfbacct,
            this.empsssno,
            this.empphno,
            this.emptin,
            this.emppagibigno,
            this.empgender,
            this.empstatus,
            this.empethnicity,
            this.position,
            this.empstartdate,
            this.empworklocation,
            this.empworkphone,
            this.contactname,
            this.contactaddress,
            this.priphone,
            this.alterphone,
            this.relationship});
            this.dgEmployeeInfo.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dgEmployeeInfo.Location = new System.Drawing.Point(18, 38);
            this.dgEmployeeInfo.Name = "dgEmployeeInfo";
            this.dgEmployeeInfo.ReadOnly = true;
            this.dgEmployeeInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEmployeeInfo.Size = new System.Drawing.Size(310, 453);
            this.dgEmployeeInfo.TabIndex = 0;
            this.dgEmployeeInfo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgEmployeeInfo_CellContentClick);
            // 
            // empid
            // 
            this.empid.HeaderText = "Employee ID";
            this.empid.Name = "empid";
            this.empid.ReadOnly = true;
            // 
            // emplastname
            // 
            this.emplastname.HeaderText = "Last Name";
            this.emplastname.Name = "emplastname";
            this.emplastname.ReadOnly = true;
            // 
            // empfirstname
            // 
            this.empfirstname.HeaderText = "First Name";
            this.empfirstname.Name = "empfirstname";
            this.empfirstname.ReadOnly = true;
            // 
            // empmidname
            // 
            this.empmidname.HeaderText = "Middle Name";
            this.empmidname.Name = "empmidname";
            this.empmidname.ReadOnly = true;
            // 
            // empmonth
            // 
            this.empmonth.HeaderText = "Month";
            this.empmonth.Name = "empmonth";
            this.empmonth.ReadOnly = true;
            // 
            // empday
            // 
            this.empday.HeaderText = "Day";
            this.empday.Name = "empday";
            this.empday.ReadOnly = true;
            // 
            // empyear
            // 
            this.empyear.HeaderText = "Year";
            this.empyear.Name = "empyear";
            this.empyear.ReadOnly = true;
            // 
            // emphomeaddress
            // 
            this.emphomeaddress.HeaderText = "Home Address";
            this.emphomeaddress.Name = "emphomeaddress";
            this.emphomeaddress.ReadOnly = true;
            // 
            // empcontactno
            // 
            this.empcontactno.HeaderText = "Contact No.";
            this.empcontactno.Name = "empcontactno";
            this.empcontactno.ReadOnly = true;
            // 
            // empemailadd
            // 
            this.empemailadd.HeaderText = "Email Address";
            this.empemailadd.Name = "empemailadd";
            this.empemailadd.ReadOnly = true;
            // 
            // empskypename
            // 
            this.empskypename.HeaderText = "Skype Name";
            this.empskypename.Name = "empskypename";
            this.empskypename.ReadOnly = true;
            // 
            // empfbacct
            // 
            this.empfbacct.HeaderText = "Facebook Acct";
            this.empfbacct.Name = "empfbacct";
            this.empfbacct.ReadOnly = true;
            // 
            // empsssno
            // 
            this.empsssno.HeaderText = "SSS No.";
            this.empsssno.Name = "empsssno";
            this.empsssno.ReadOnly = true;
            // 
            // empphno
            // 
            this.empphno.HeaderText = "PhilHealth No.";
            this.empphno.Name = "empphno";
            this.empphno.ReadOnly = true;
            // 
            // emptin
            // 
            this.emptin.HeaderText = "TIN";
            this.emptin.Name = "emptin";
            this.emptin.ReadOnly = true;
            // 
            // emppagibigno
            // 
            this.emppagibigno.HeaderText = "Pag-ibig No.";
            this.emppagibigno.Name = "emppagibigno";
            this.emppagibigno.ReadOnly = true;
            // 
            // empgender
            // 
            this.empgender.HeaderText = "Gender";
            this.empgender.Name = "empgender";
            this.empgender.ReadOnly = true;
            // 
            // empstatus
            // 
            this.empstatus.HeaderText = "Status";
            this.empstatus.Name = "empstatus";
            this.empstatus.ReadOnly = true;
            // 
            // empethnicity
            // 
            this.empethnicity.HeaderText = "Ethnicity";
            this.empethnicity.Name = "empethnicity";
            this.empethnicity.ReadOnly = true;
            // 
            // position
            // 
            this.position.HeaderText = "Position";
            this.position.Name = "position";
            this.position.ReadOnly = true;
            // 
            // empstartdate
            // 
            this.empstartdate.HeaderText = "Start Date";
            this.empstartdate.Name = "empstartdate";
            this.empstartdate.ReadOnly = true;
            // 
            // empworklocation
            // 
            this.empworklocation.HeaderText = "Work Location";
            this.empworklocation.Name = "empworklocation";
            this.empworklocation.ReadOnly = true;
            // 
            // empworkphone
            // 
            this.empworkphone.HeaderText = "Work Phone";
            this.empworkphone.Name = "empworkphone";
            this.empworkphone.ReadOnly = true;
            // 
            // contactname
            // 
            this.contactname.HeaderText = "Contact Name";
            this.contactname.Name = "contactname";
            this.contactname.ReadOnly = true;
            // 
            // contactaddress
            // 
            this.contactaddress.HeaderText = "Contact Address";
            this.contactaddress.Name = "contactaddress";
            this.contactaddress.ReadOnly = true;
            // 
            // priphone
            // 
            this.priphone.HeaderText = "Primary Phone";
            this.priphone.Name = "priphone";
            this.priphone.ReadOnly = true;
            // 
            // alterphone
            // 
            this.alterphone.HeaderText = "Alternate Phone";
            this.alterphone.Name = "alterphone";
            this.alterphone.ReadOnly = true;
            // 
            // relationship
            // 
            this.relationship.HeaderText = "Relationship";
            this.relationship.Name = "relationship";
            this.relationship.ReadOnly = true;
            // 
            // btnSaveEmpInfo
            // 
            this.btnSaveEmpInfo.Enabled = false;
            this.btnSaveEmpInfo.Location = new System.Drawing.Point(1201, 470);
            this.btnSaveEmpInfo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSaveEmpInfo.Name = "btnSaveEmpInfo";
            this.btnSaveEmpInfo.Size = new System.Drawing.Size(77, 28);
            this.btnSaveEmpInfo.TabIndex = 51;
            this.btnSaveEmpInfo.Text = "SAVE";
            this.btnSaveEmpInfo.UseVisualStyleBackColor = true;
            this.btnSaveEmpInfo.Click += new System.EventHandler(this.btnSaveEmpInfo_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.txtrelationship);
            this.groupBox4.Controls.Add(this.txtalterphone);
            this.groupBox4.Controls.Add(this.txtpriphone);
            this.groupBox4.Controls.Add(this.txtcontactaddress);
            this.groupBox4.Controls.Add(this.txtcontactname);
            this.groupBox4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox4.Location = new System.Drawing.Point(999, 219);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.groupBox4.Size = new System.Drawing.Size(334, 247);
            this.groupBox4.TabIndex = 48;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "EMERGENCY CONTACT";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(12, 39);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(89, 15);
            this.label24.TabIndex = 44;
            this.label24.Text = "Contact Name:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(14, 211);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(78, 15);
            this.label23.TabIndex = 43;
            this.label23.Text = "Relationship:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(12, 74);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(100, 15);
            this.label22.TabIndex = 42;
            this.label22.Text = "Contact Address:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(14, 142);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(92, 15);
            this.label21.TabIndex = 41;
            this.label21.Text = "Primary Phone:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(14, 177);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(102, 15);
            this.label20.TabIndex = 40;
            this.label20.Text = "Alternate Phone:";
            // 
            // txtrelationship
            // 
            this.txtrelationship.Enabled = false;
            this.txtrelationship.Location = new System.Drawing.Point(116, 201);
            this.txtrelationship.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtrelationship.Name = "txtrelationship";
            this.txtrelationship.Size = new System.Drawing.Size(188, 23);
            this.txtrelationship.TabIndex = 39;
            // 
            // txtalterphone
            // 
            this.txtalterphone.Enabled = false;
            this.txtalterphone.Location = new System.Drawing.Point(116, 167);
            this.txtalterphone.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtalterphone.Name = "txtalterphone";
            this.txtalterphone.Size = new System.Drawing.Size(188, 23);
            this.txtalterphone.TabIndex = 38;
            // 
            // txtpriphone
            // 
            this.txtpriphone.Enabled = false;
            this.txtpriphone.Location = new System.Drawing.Point(116, 132);
            this.txtpriphone.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtpriphone.Name = "txtpriphone";
            this.txtpriphone.Size = new System.Drawing.Size(188, 23);
            this.txtpriphone.TabIndex = 37;
            // 
            // txtcontactaddress
            // 
            this.txtcontactaddress.Enabled = false;
            this.txtcontactaddress.Location = new System.Drawing.Point(116, 69);
            this.txtcontactaddress.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtcontactaddress.Multiline = true;
            this.txtcontactaddress.Name = "txtcontactaddress";
            this.txtcontactaddress.Size = new System.Drawing.Size(186, 54);
            this.txtcontactaddress.TabIndex = 36;
            // 
            // txtcontactname
            // 
            this.txtcontactname.Enabled = false;
            this.txtcontactname.Location = new System.Drawing.Point(116, 39);
            this.txtcontactname.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtcontactname.Name = "txtcontactname";
            this.txtcontactname.Size = new System.Drawing.Size(186, 23);
            this.txtcontactname.TabIndex = 35;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox3.Controls.Add(this.txtstartdate);
            this.groupBox3.Controls.Add(this.txtworklocation);
            this.groupBox3.Controls.Add(this.txtphone);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.txtposition);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox3.Location = new System.Drawing.Point(999, 12);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.groupBox3.Size = new System.Drawing.Size(334, 205);
            this.groupBox3.TabIndex = 47;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "JOB INFORMATION";
            // 
            // txtstartdate
            // 
            this.txtstartdate.Enabled = false;
            this.txtstartdate.Location = new System.Drawing.Point(105, 69);
            this.txtstartdate.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtstartdate.Name = "txtstartdate";
            this.txtstartdate.Size = new System.Drawing.Size(188, 23);
            this.txtstartdate.TabIndex = 40;
            // 
            // txtworklocation
            // 
            this.txtworklocation.Enabled = false;
            this.txtworklocation.Location = new System.Drawing.Point(105, 103);
            this.txtworklocation.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtworklocation.Multiline = true;
            this.txtworklocation.Name = "txtworklocation";
            this.txtworklocation.Size = new System.Drawing.Size(214, 52);
            this.txtworklocation.TabIndex = 39;
            // 
            // txtphone
            // 
            this.txtphone.Enabled = false;
            this.txtphone.Location = new System.Drawing.Point(105, 165);
            this.txtphone.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtphone.Name = "txtphone";
            this.txtphone.Size = new System.Drawing.Size(214, 23);
            this.txtphone.TabIndex = 38;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(13, 171);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(78, 15);
            this.label19.TabIndex = 37;
            this.label19.Text = "Work Phone:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(14, 100);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(89, 15);
            this.label18.TabIndex = 36;
            this.label18.Text = "Work Location:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 73);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 15);
            this.label17.TabIndex = 35;
            this.label17.Text = "Start Date:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(13, 40);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 15);
            this.label16.TabIndex = 34;
            this.label16.Text = "Title / Position:";
            // 
            // txtposition
            // 
            this.txtposition.Enabled = false;
            this.txtposition.Location = new System.Drawing.Point(105, 34);
            this.txtposition.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtposition.Name = "txtposition";
            this.txtposition.Size = new System.Drawing.Size(214, 23);
            this.txtposition.TabIndex = 34;
            // 
            // txtempid
            // 
            this.txtempid.Enabled = false;
            this.txtempid.Location = new System.Drawing.Point(442, 8);
            this.txtempid.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtempid.Name = "txtempid";
            this.txtempid.Size = new System.Drawing.Size(146, 23);
            this.txtempid.TabIndex = 46;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(337, 10);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(97, 15);
            this.label15.TabIndex = 45;
            this.label15.Text = "Employee ID No.";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox1.Controls.Add(this.txtyear);
            this.groupBox1.Controls.Add(this.cmbempbirthday);
            this.groupBox1.Controls.Add(this.cmbempbirthyear);
            this.groupBox1.Controls.Add(this.cmbempbirthmonth);
            this.groupBox1.Controls.Add(this.txtmonth);
            this.groupBox1.Controls.Add(this.txtday);
            this.groupBox1.Controls.Add(this.txtpagibigno);
            this.groupBox1.Controls.Add(this.txtphno);
            this.groupBox1.Controls.Add(this.txttin);
            this.groupBox1.Controls.Add(this.txtssno);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtfbacct);
            this.groupBox1.Controls.Add(this.txtskypename);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtemailaddress);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txthomeaddress);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtcontactno);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtempmidname);
            this.groupBox1.Controls.Add(this.txtemplastname);
            this.groupBox1.Controls.Add(this.txtempfirstname);
            this.groupBox1.Location = new System.Drawing.Point(336, 38);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.groupBox1.Size = new System.Drawing.Size(653, 285);
            this.groupBox1.TabIndex = 43;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PERSONAL INFORMATION";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // txtyear
            // 
            this.txtyear.Enabled = false;
            this.txtyear.Location = new System.Drawing.Point(415, 53);
            this.txtyear.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtyear.Name = "txtyear";
            this.txtyear.Size = new System.Drawing.Size(56, 23);
            this.txtyear.TabIndex = 34;
            // 
            // cmbempbirthday
            // 
            this.cmbempbirthday.Enabled = false;
            this.cmbempbirthday.FormattingEnabled = true;
            this.cmbempbirthday.Location = new System.Drawing.Point(349, 53);
            this.cmbempbirthday.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmbempbirthday.Name = "cmbempbirthday";
            this.cmbempbirthday.Size = new System.Drawing.Size(57, 23);
            this.cmbempbirthday.TabIndex = 44;
            this.cmbempbirthday.Text = "Day";
            // 
            // cmbempbirthyear
            // 
            this.cmbempbirthyear.Enabled = false;
            this.cmbempbirthyear.FormattingEnabled = true;
            this.cmbempbirthyear.Location = new System.Drawing.Point(470, 53);
            this.cmbempbirthyear.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmbempbirthyear.Name = "cmbempbirthyear";
            this.cmbempbirthyear.Size = new System.Drawing.Size(52, 23);
            this.cmbempbirthyear.TabIndex = 43;
            this.cmbempbirthyear.Text = "Year";
            // 
            // cmbempbirthmonth
            // 
            this.cmbempbirthmonth.Enabled = false;
            this.cmbempbirthmonth.FormattingEnabled = true;
            this.cmbempbirthmonth.Location = new System.Drawing.Point(243, 53);
            this.cmbempbirthmonth.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmbempbirthmonth.Name = "cmbempbirthmonth";
            this.cmbempbirthmonth.Size = new System.Drawing.Size(64, 23);
            this.cmbempbirthmonth.TabIndex = 42;
            this.cmbempbirthmonth.Text = "Month";
            this.cmbempbirthmonth.SelectedIndexChanged += new System.EventHandler(this.cmbempbirthmonth_SelectedIndexChanged);
            // 
            // txtmonth
            // 
            this.txtmonth.Enabled = false;
            this.txtmonth.Location = new System.Drawing.Point(152, 53);
            this.txtmonth.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtmonth.Name = "txtmonth";
            this.txtmonth.Size = new System.Drawing.Size(94, 23);
            this.txtmonth.TabIndex = 36;
            this.txtmonth.TextChanged += new System.EventHandler(this.txtmonth_TextChanged);
            // 
            // txtday
            // 
            this.txtday.Enabled = false;
            this.txtday.Location = new System.Drawing.Point(319, 53);
            this.txtday.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtday.Name = "txtday";
            this.txtday.Size = new System.Drawing.Size(31, 23);
            this.txtday.TabIndex = 35;
            // 
            // txtpagibigno
            // 
            this.txtpagibigno.Enabled = false;
            this.txtpagibigno.Location = new System.Drawing.Point(478, 239);
            this.txtpagibigno.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtpagibigno.Name = "txtpagibigno";
            this.txtpagibigno.Size = new System.Drawing.Size(156, 23);
            this.txtpagibigno.TabIndex = 33;
            // 
            // txtphno
            // 
            this.txtphno.Enabled = false;
            this.txtphno.Location = new System.Drawing.Point(478, 181);
            this.txtphno.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtphno.Name = "txtphno";
            this.txtphno.Size = new System.Drawing.Size(156, 23);
            this.txtphno.TabIndex = 32;
            // 
            // txttin
            // 
            this.txttin.Enabled = false;
            this.txttin.Location = new System.Drawing.Point(478, 210);
            this.txttin.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txttin.Name = "txttin";
            this.txttin.Size = new System.Drawing.Size(156, 23);
            this.txttin.TabIndex = 31;
            // 
            // txtssno
            // 
            this.txtssno.Enabled = false;
            this.txtssno.Location = new System.Drawing.Point(478, 153);
            this.txtssno.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtssno.Name = "txtssno";
            this.txtssno.Size = new System.Drawing.Size(156, 23);
            this.txtssno.TabIndex = 30;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(317, 242);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 15);
            this.label11.TabIndex = 29;
            this.label11.Text = "Pag-ibig No:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(317, 218);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(151, 15);
            this.label10.TabIndex = 28;
            this.label10.Text = "Tax Identification No.(TIN):";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(317, 190);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 15);
            this.label9.TabIndex = 27;
            this.label9.Text = "Phil Health No:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(317, 160);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 15);
            this.label8.TabIndex = 26;
            this.label8.Text = "Social Security No.(SSS):";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 242);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 15);
            this.label7.TabIndex = 25;
            this.label7.Text = "Facebook Acct:";
            // 
            // txtfbacct
            // 
            this.txtfbacct.Enabled = false;
            this.txtfbacct.Location = new System.Drawing.Point(152, 239);
            this.txtfbacct.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtfbacct.Name = "txtfbacct";
            this.txtfbacct.Size = new System.Drawing.Size(156, 23);
            this.txtfbacct.TabIndex = 24;
            // 
            // txtskypename
            // 
            this.txtskypename.Enabled = false;
            this.txtskypename.Location = new System.Drawing.Point(152, 210);
            this.txtskypename.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtskypename.Name = "txtskypename";
            this.txtskypename.Size = new System.Drawing.Size(156, 23);
            this.txtskypename.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 215);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 15);
            this.label6.TabIndex = 22;
            this.label6.Text = "Skype Acct Name:";
            // 
            // txtemailaddress
            // 
            this.txtemailaddress.Enabled = false;
            this.txtemailaddress.Location = new System.Drawing.Point(152, 181);
            this.txtemailaddress.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtemailaddress.Name = "txtemailaddress";
            this.txtemailaddress.Size = new System.Drawing.Size(156, 23);
            this.txtemailaddress.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 184);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 15);
            this.label4.TabIndex = 20;
            this.label4.Text = "Email Address:";
            // 
            // txthomeaddress
            // 
            this.txthomeaddress.Enabled = false;
            this.txthomeaddress.Location = new System.Drawing.Point(151, 88);
            this.txthomeaddress.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txthomeaddress.Multiline = true;
            this.txthomeaddress.Name = "txthomeaddress";
            this.txthomeaddress.Size = new System.Drawing.Size(483, 57);
            this.txthomeaddress.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 83);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 15);
            this.label3.TabIndex = 18;
            this.label3.Text = "Home Address:";
            // 
            // txtcontactno
            // 
            this.txtcontactno.Enabled = false;
            this.txtcontactno.Location = new System.Drawing.Point(152, 152);
            this.txtcontactno.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtcontactno.Name = "txtcontactno";
            this.txtcontactno.Size = new System.Drawing.Size(156, 23);
            this.txtcontactno.TabIndex = 17;
            this.txtcontactno.TextChanged += new System.EventHandler(this.txtcontactno_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 155);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 15);
            this.label2.TabIndex = 16;
            this.label2.Text = "Contact Number/s:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 61);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 15);
            this.label5.TabIndex = 7;
            this.label5.Text = "Date of Birth:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Employee Name:";
            // 
            // txtempmidname
            // 
            this.txtempmidname.Enabled = false;
            this.txtempmidname.Location = new System.Drawing.Point(441, 24);
            this.txtempmidname.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtempmidname.Name = "txtempmidname";
            this.txtempmidname.Size = new System.Drawing.Size(135, 23);
            this.txtempmidname.TabIndex = 2;
            this.txtempmidname.Text = "Middle Name";
            // 
            // txtemplastname
            // 
            this.txtemplastname.Enabled = false;
            this.txtemplastname.Location = new System.Drawing.Point(152, 24);
            this.txtemplastname.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtemplastname.Name = "txtemplastname";
            this.txtemplastname.Size = new System.Drawing.Size(134, 23);
            this.txtemplastname.TabIndex = 1;
            this.txtemplastname.Text = "Last Name";
            // 
            // txtempfirstname
            // 
            this.txtempfirstname.Enabled = false;
            this.txtempfirstname.Location = new System.Drawing.Point(296, 24);
            this.txtempfirstname.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtempfirstname.Name = "txtempfirstname";
            this.txtempfirstname.Size = new System.Drawing.Size(135, 23);
            this.txtempfirstname.TabIndex = 0;
            this.txtempfirstname.Text = "First Name";
            // 
            // txtgender
            // 
            this.txtgender.Enabled = false;
            this.txtgender.Location = new System.Drawing.Point(62, 14);
            this.txtgender.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtgender.Name = "txtgender";
            this.txtgender.Size = new System.Drawing.Size(64, 23);
            this.txtgender.TabIndex = 41;
            // 
            // txtstatus
            // 
            this.txtstatus.Enabled = false;
            this.txtstatus.Location = new System.Drawing.Point(106, 15);
            this.txtstatus.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtstatus.Name = "txtstatus";
            this.txtstatus.Size = new System.Drawing.Size(81, 23);
            this.txtstatus.TabIndex = 40;
            // 
            // txtethnicity
            // 
            this.txtethnicity.Enabled = false;
            this.txtethnicity.Location = new System.Drawing.Point(72, 15);
            this.txtethnicity.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtethnicity.Name = "txtethnicity";
            this.txtethnicity.Size = new System.Drawing.Size(179, 23);
            this.txtethnicity.TabIndex = 39;
            // 
            // btnEditEmployeeInfo
            // 
            this.btnEditEmployeeInfo.Location = new System.Drawing.Point(1066, 470);
            this.btnEditEmployeeInfo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEditEmployeeInfo.Name = "btnEditEmployeeInfo";
            this.btnEditEmployeeInfo.Size = new System.Drawing.Size(77, 28);
            this.btnEditEmployeeInfo.TabIndex = 52;
            this.btnEditEmployeeInfo.Text = "EDIT";
            this.btnEditEmployeeInfo.UseVisualStyleBackColor = true;
            this.btnEditEmployeeInfo.Click += new System.EventHandler(this.btnEditEmployeeInfo_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(780, 470);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(209, 15);
            this.label25.TabIndex = 42;
            this.label25.Text = "*Please type N/A if NOT APPLICABLE.";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox6.Controls.Add(this.rbafrican);
            this.groupBox6.Controls.Add(this.rbasian);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.txtethnicity);
            this.groupBox6.Controls.Add(this.rbhispanic);
            this.groupBox6.Controls.Add(this.rbalaskan);
            this.groupBox6.Controls.Add(this.rbwhite);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Location = new System.Drawing.Point(733, 324);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Size = new System.Drawing.Size(258, 134);
            this.groupBox6.TabIndex = 47;
            this.groupBox6.TabStop = false;
            // 
            // rbafrican
            // 
            this.rbafrican.AutoSize = true;
            this.rbafrican.Location = new System.Drawing.Point(18, 41);
            this.rbafrican.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.rbafrican.Name = "rbafrican";
            this.rbafrican.Size = new System.Drawing.Size(125, 19);
            this.rbafrican.TabIndex = 31;
            this.rbafrican.TabStop = true;
            this.rbafrican.Text = "African - American";
            this.rbafrican.UseVisualStyleBackColor = true;
            this.rbafrican.CheckedChanged += new System.EventHandler(this.rbafrican_CheckedChanged);
            // 
            // rbasian
            // 
            this.rbasian.AutoSize = true;
            this.rbasian.Location = new System.Drawing.Point(18, 57);
            this.rbasian.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.rbasian.Name = "rbasian";
            this.rbasian.Size = new System.Drawing.Size(145, 19);
            this.rbasian.TabIndex = 30;
            this.rbasian.TabStop = true;
            this.rbasian.Text = "Asian / Pacific Islander";
            this.rbasian.UseVisualStyleBackColor = true;
            this.rbasian.CheckedChanged += new System.EventHandler(this.rbasian_CheckedChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(14, 21);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(58, 15);
            this.label26.TabIndex = 26;
            this.label26.Text = "Ethnicity:";
            // 
            // rbhispanic
            // 
            this.rbhispanic.AutoSize = true;
            this.rbhispanic.Location = new System.Drawing.Point(18, 73);
            this.rbhispanic.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.rbhispanic.Name = "rbhispanic";
            this.rbhispanic.Size = new System.Drawing.Size(69, 19);
            this.rbhispanic.TabIndex = 34;
            this.rbhispanic.TabStop = true;
            this.rbhispanic.Text = "Hispanic";
            this.rbhispanic.UseVisualStyleBackColor = true;
            this.rbhispanic.CheckedChanged += new System.EventHandler(this.rbhispanic_CheckedChanged);
            // 
            // rbalaskan
            // 
            this.rbalaskan.AutoSize = true;
            this.rbalaskan.Location = new System.Drawing.Point(18, 106);
            this.rbalaskan.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.rbalaskan.Name = "rbalaskan";
            this.rbalaskan.Size = new System.Drawing.Size(208, 19);
            this.rbalaskan.TabIndex = 37;
            this.rbalaskan.TabStop = true;
            this.rbalaskan.Text = "Alaskan Native / Native American";
            this.rbalaskan.UseVisualStyleBackColor = true;
            this.rbalaskan.CheckedChanged += new System.EventHandler(this.rbalaskan_CheckedChanged);
            // 
            // rbwhite
            // 
            this.rbwhite.AutoSize = true;
            this.rbwhite.Location = new System.Drawing.Point(18, 89);
            this.rbwhite.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.rbwhite.Name = "rbwhite";
            this.rbwhite.Size = new System.Drawing.Size(148, 19);
            this.rbwhite.TabIndex = 36;
            this.rbwhite.TabStop = true;
            this.rbwhite.Text = "White / Non - Hispanic";
            this.rbwhite.UseVisualStyleBackColor = true;
            this.rbwhite.CheckedChanged += new System.EventHandler(this.rbwhite_CheckedChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(32, 111);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(190, 15);
            this.label27.TabIndex = 42;
            this.label27.Text = "Alaskan Native / Native American";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox5.Controls.Add(this.rbseparated);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this.rbdivorced);
            this.groupBox5.Controls.Add(this.txtstatus);
            this.groupBox5.Controls.Add(this.rbsingle);
            this.groupBox5.Controls.Add(this.rbmarried);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Location = new System.Drawing.Point(516, 324);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(196, 133);
            this.groupBox5.TabIndex = 46;
            this.groupBox5.TabStop = false;
            // 
            // rbseparated
            // 
            this.rbseparated.AutoSize = true;
            this.rbseparated.Location = new System.Drawing.Point(23, 86);
            this.rbseparated.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.rbseparated.Name = "rbseparated";
            this.rbseparated.Size = new System.Drawing.Size(81, 19);
            this.rbseparated.TabIndex = 35;
            this.rbseparated.TabStop = true;
            this.rbseparated.Text = "Separated";
            this.rbseparated.UseVisualStyleBackColor = true;
            this.rbseparated.CheckedChanged += new System.EventHandler(this.rbseparated_CheckedChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(19, 18);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(87, 15);
            this.label28.TabIndex = 27;
            this.label28.Text = "Marital Status:";
            // 
            // rbdivorced
            // 
            this.rbdivorced.AutoSize = true;
            this.rbdivorced.Location = new System.Drawing.Point(23, 70);
            this.rbdivorced.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.rbdivorced.Name = "rbdivorced";
            this.rbdivorced.Size = new System.Drawing.Size(73, 19);
            this.rbdivorced.TabIndex = 32;
            this.rbdivorced.TabStop = true;
            this.rbdivorced.Text = "Divorced";
            this.rbdivorced.UseVisualStyleBackColor = true;
            this.rbdivorced.CheckedChanged += new System.EventHandler(this.rbdivorced_CheckedChanged);
            // 
            // rbsingle
            // 
            this.rbsingle.AutoSize = true;
            this.rbsingle.Location = new System.Drawing.Point(23, 38);
            this.rbsingle.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.rbsingle.Name = "rbsingle";
            this.rbsingle.Size = new System.Drawing.Size(57, 19);
            this.rbsingle.TabIndex = 38;
            this.rbsingle.TabStop = true;
            this.rbsingle.Text = "Single";
            this.rbsingle.UseVisualStyleBackColor = true;
            this.rbsingle.CheckedChanged += new System.EventHandler(this.rbsingle_CheckedChanged);
            // 
            // rbmarried
            // 
            this.rbmarried.AutoSize = true;
            this.rbmarried.Location = new System.Drawing.Point(23, 54);
            this.rbmarried.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.rbmarried.Name = "rbmarried";
            this.rbmarried.Size = new System.Drawing.Size(69, 19);
            this.rbmarried.TabIndex = 33;
            this.rbmarried.TabStop = true;
            this.rbmarried.Text = "Married";
            this.rbmarried.UseVisualStyleBackColor = true;
            this.rbmarried.CheckedChanged += new System.EventHandler(this.rbmarried_CheckedChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(37, 90);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(63, 15);
            this.label29.TabIndex = 41;
            this.label29.Text = "Separated";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox2.Controls.Add(this.rbfemale);
            this.groupBox2.Controls.Add(this.rbmale);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.txtgender);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox2.Location = new System.Drawing.Point(337, 325);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Size = new System.Drawing.Size(159, 133);
            this.groupBox2.TabIndex = 45;
            this.groupBox2.TabStop = false;
            // 
            // rbfemale
            // 
            this.rbfemale.AutoSize = true;
            this.rbfemale.Location = new System.Drawing.Point(13, 55);
            this.rbfemale.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.rbfemale.Name = "rbfemale";
            this.rbfemale.Size = new System.Drawing.Size(65, 19);
            this.rbfemale.TabIndex = 39;
            this.rbfemale.TabStop = true;
            this.rbfemale.Text = "Female";
            this.rbfemale.UseVisualStyleBackColor = true;
            this.rbfemale.CheckedChanged += new System.EventHandler(this.rbfemale_CheckedChanged);
            // 
            // rbmale
            // 
            this.rbmale.AutoSize = true;
            this.rbmale.Location = new System.Drawing.Point(12, 38);
            this.rbmale.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.rbmale.Name = "rbmale";
            this.rbmale.Size = new System.Drawing.Size(52, 19);
            this.rbmale.TabIndex = 29;
            this.rbmale.TabStop = true;
            this.rbmale.Text = "Male";
            this.rbmale.UseVisualStyleBackColor = true;
            this.rbmale.CheckedChanged += new System.EventHandler(this.rbmale_CheckedChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(9, 18);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(52, 15);
            this.label30.TabIndex = 28;
            this.label30.Text = "Gender:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(30, 57);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(45, 15);
            this.label31.TabIndex = 40;
            this.label31.Text = "female";
            // 
            // ViewUpdateEmployeeInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(1342, 504);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.btnEditEmployeeInfo);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnSaveEmpInfo);
            this.Controls.Add(this.txtempid);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgEmployeeInfo);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ViewUpdateEmployeeInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ViewUpdateEmployeeInfo";
            this.Load += new System.EventHandler(this.ViewUpdateEmployeeInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgEmployeeInfo)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgEmployeeInfo;
        private System.Windows.Forms.Button btnSaveEmpInfo;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtrelationship;
        private System.Windows.Forms.TextBox txtalterphone;
        private System.Windows.Forms.TextBox txtpriphone;
        private System.Windows.Forms.TextBox txtcontactaddress;
        private System.Windows.Forms.TextBox txtcontactname;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtstartdate;
        private System.Windows.Forms.TextBox txtworklocation;
        private System.Windows.Forms.TextBox txtphone;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtposition;
        private System.Windows.Forms.TextBox txtempid;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtpagibigno;
        private System.Windows.Forms.TextBox txtphno;
        private System.Windows.Forms.TextBox txttin;
        private System.Windows.Forms.TextBox txtssno;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtfbacct;
        private System.Windows.Forms.TextBox txtskypename;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtemailaddress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txthomeaddress;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtcontactno;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtempmidname;
        private System.Windows.Forms.TextBox txtemplastname;
        private System.Windows.Forms.TextBox txtempfirstname;
        private System.Windows.Forms.Button btnEditEmployeeInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn empid;
        private System.Windows.Forms.DataGridViewTextBoxColumn emplastname;
        private System.Windows.Forms.DataGridViewTextBoxColumn empfirstname;
        private System.Windows.Forms.DataGridViewTextBoxColumn empmidname;
        private System.Windows.Forms.DataGridViewTextBoxColumn empmonth;
        private System.Windows.Forms.DataGridViewTextBoxColumn empday;
        private System.Windows.Forms.DataGridViewTextBoxColumn empyear;
        private System.Windows.Forms.DataGridViewTextBoxColumn emphomeaddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn empcontactno;
        private System.Windows.Forms.DataGridViewTextBoxColumn empemailadd;
        private System.Windows.Forms.DataGridViewTextBoxColumn empskypename;
        private System.Windows.Forms.DataGridViewTextBoxColumn empfbacct;
        private System.Windows.Forms.DataGridViewTextBoxColumn empsssno;
        private System.Windows.Forms.DataGridViewTextBoxColumn empphno;
        private System.Windows.Forms.DataGridViewTextBoxColumn emptin;
        private System.Windows.Forms.DataGridViewTextBoxColumn emppagibigno;
        private System.Windows.Forms.DataGridViewTextBoxColumn empgender;
        private System.Windows.Forms.DataGridViewTextBoxColumn empstatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn empethnicity;
        private System.Windows.Forms.DataGridViewTextBoxColumn position;
        private System.Windows.Forms.DataGridViewTextBoxColumn empstartdate;
        private System.Windows.Forms.DataGridViewTextBoxColumn empworklocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn empworkphone;
        private System.Windows.Forms.DataGridViewTextBoxColumn contactname;
        private System.Windows.Forms.DataGridViewTextBoxColumn contactaddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn priphone;
        private System.Windows.Forms.DataGridViewTextBoxColumn alterphone;
        private System.Windows.Forms.DataGridViewTextBoxColumn relationship;
        private System.Windows.Forms.TextBox txtgender;
        private System.Windows.Forms.TextBox txtstatus;
        private System.Windows.Forms.TextBox txtethnicity;
        private System.Windows.Forms.TextBox txtmonth;
        private System.Windows.Forms.TextBox txtday;
        private System.Windows.Forms.TextBox txtyear;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox cmbempbirthday;
        private System.Windows.Forms.ComboBox cmbempbirthyear;
        private System.Windows.Forms.ComboBox cmbempbirthmonth;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton rbafrican;
        private System.Windows.Forms.RadioButton rbasian;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.RadioButton rbhispanic;
        private System.Windows.Forms.RadioButton rbalaskan;
        private System.Windows.Forms.RadioButton rbwhite;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rbseparated;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.RadioButton rbdivorced;
        private System.Windows.Forms.RadioButton rbsingle;
        private System.Windows.Forms.RadioButton rbmarried;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbfemale;
        private System.Windows.Forms.RadioButton rbmale;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
    }
}