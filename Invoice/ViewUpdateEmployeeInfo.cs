﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Invoice
{
    public partial class ViewUpdateEmployeeInfo : Form
    {
        public ViewUpdateEmployeeInfo()
        {
            InitializeComponent();
        }

        public static MongoCollection<EmployeeEntity> Employees = new MongoClient(@"mongodb://client144:client144devnetworks@144.76.166.207/DevNet").GetServer().GetDatabase("DevNet").GetCollection<EmployeeEntity>("EmployeeInfo");

        private void ViewUpdateEmployeeInfo_Load(object sender, EventArgs e)
        {

            var months = System.Globalization.DateTimeFormatInfo.InvariantInfo.MonthNames;
            cmbempbirthmonth.DataSource = months;

            var day = Enumerable.Range(1, 30).ToList();
            cmbempbirthday.DataSource = day;

            var year = Enumerable.Range(1950, DateTime.Now.Year).ToList();
            cmbempbirthyear.DataSource = year;

            var employeeinfo = Employees.FindAll();

            foreach (var showemployeeinfo in employeeinfo)
            {
                dgEmployeeInfo.Rows.Add(showemployeeinfo.empid,
                                        showemployeeinfo.emplastname,
                                        showemployeeinfo.empfirstname,
                                        showemployeeinfo.empmiddlename,
                                        showemployeeinfo.empbirthmonth,
                                        showemployeeinfo.empbirthday,
                                        showemployeeinfo.empbirthyear,
                                        showemployeeinfo.emphomeaddress,
                                        showemployeeinfo.empcontactno,
                                        showemployeeinfo.empemailaddress,
                                        showemployeeinfo.empskypename,
                                        showemployeeinfo.empfbacct,
                                        showemployeeinfo.empSSSNo,
                                        showemployeeinfo.empPhilHealthNo,
                                        showemployeeinfo.empTIN,
                                        showemployeeinfo.empPagIbigNo,
                                        showemployeeinfo.empgender,
                                        showemployeeinfo.empstatus,
                                        showemployeeinfo.empethnicity,
                                        showemployeeinfo.empposition,
                                        showemployeeinfo.empstartdate.ToShortDateString(),
                                        showemployeeinfo.empworklocation,
                                        showemployeeinfo.empworkphone,
                                        showemployeeinfo.contactname,
                                        showemployeeinfo.contactaddress,
                                        showemployeeinfo.primaryphone,
                                        showemployeeinfo.alterphone,
                                        showemployeeinfo.relationship);
            }
        }

        private void dgEmployeeInfo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dgEmployeeInfo.Rows[e.RowIndex];

                txtempid.Text = row.Cells["empid"].Value.ToString();
                txtemplastname.Text = row.Cells["emplastname"].Value.ToString();
                txtempfirstname.Text = row.Cells["empfirstname"].Value.ToString();
                txtempmidname.Text = row.Cells["empmidname"].Value.ToString();
                txtmonth.Text = row.Cells["empmonth"].Value.ToString();
                txtday.Text = row.Cells["empday"].Value.ToString();
                txtyear.Text = row.Cells["empyear"].Value.ToString();
                txthomeaddress.Text = row.Cells["emphomeaddress"].Value.ToString();
                txtcontactno.Text = row.Cells["empcontactno"].Value.ToString();
                txtemailaddress.Text = row.Cells["empemailadd"].Value.ToString();
                txtskypename.Text = row.Cells["empskypename"].Value.ToString();
                txtfbacct.Text = row.Cells["empfbacct"].Value.ToString();
                txtssno.Text = row.Cells["empsssno"].Value.ToString();
                txtphno.Text = row.Cells["empphno"].Value.ToString();
                txttin.Text = row.Cells["emptin"].Value.ToString();
                txtpagibigno.Text = row.Cells["emppagibigno"].Value.ToString();
                txtgender.Text = row.Cells["empgender"].Value.ToString();
                txtstatus.Text = row.Cells["empstatus"].Value.ToString();
                txtethnicity.Text = row.Cells["empethnicity"].Value.ToString();
                txtposition.Text = row.Cells["position"].Value.ToString();
                txtstartdate.Text = row.Cells["empstartdate"].Value.ToString();
                txtworklocation.Text = row.Cells["empworklocation"].Value.ToString(); 
                txtphone.Text = row.Cells["empworkphone"].Value.ToString();
                txtcontactname.Text = row.Cells["contactname"].Value.ToString();
                txtcontactaddress.Text = row.Cells["contactaddress"].Value.ToString();
                txtpriphone.Text = row.Cells["priphone"].Value.ToString();
                txtalterphone.Text = row.Cells["alterphone"].Value.ToString();
                txtrelationship.Text = row.Cells["relationship"].Value.ToString();
            }
        }

        private void btnSaveEmpInfo_Click(object sender, EventArgs e)
        {
            UpdateBuilder update = MongoDB.Driver.Builders.Update.Set("emplastname", txtemplastname.Text)
                                                .Set("empfirstname", txtempfirstname.Text)
                                                .Set("empmiddlename", txtempmidname.Text)
                                                .Set("empbirthmonth", txtmonth.Text)
                                                .Set("empbirthday", txtday.Text)
                                                .Set("empbirthyear", txtyear.Text)
                                                .Set("emphomeaddress", txthomeaddress.Text)
                                                .Set("empcontactno", txtcontactno.Text)
                                                .Set("empemailaddress", txtemailaddress.Text)
                                                .Set("empskypename", txtskypename.Text)
                                                .Set("empfbacct", txtfbacct.Text)
                                                .Set("empSSSNo", txtssno.Text)
                                                .Set("empPhilHealthNo", txtphno.Text)
                                                .Set("empTIN", txttin.Text)
                                                .Set("empPagIbigNo", txtpagibigno.Text)
                                                .Set("empgender", txtgender.Text)
                                                .Set("empstatus", txtstatus.Text)
                                                .Set("empethnicity", txtethnicity.Text)
                                                .Set("empposition", txtposition.Text)
                                                .Set("empstartdate", txtstartdate.Text)
                                                .Set("empworklocation", txtworklocation.Text)
                                                .Set("empworkphone", txtphone.Text)
                                                .Set("contactname", txtcontactname.Text)
                                                .Set("contactaddress", txtcontactaddress.Text)
                                                .Set("primaryphone", txtpriphone.Text)
                                                .Set("alterphone", txtalterphone.Text)
                                                .Set("relationship", txtrelationship.Text)
                                                ;

            Employees.Update(Query.EQ("_id", txtempid.Text), update);
            MessageBox.Show("Successfully Updated!", "Update Employee Information");
        }

        private void txtmonth_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmbempbirthmonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtmonth.Text = cmbempbirthmonth.Text;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void rbmale_CheckedChanged(object sender, EventArgs e)
        {
            if (rbmale.Checked)
            { txtgender.Text = rbmale.Text; }
            else if (rbfemale.Checked)
            { txtgender.Text = rbfemale.Text; }
        }

        private void rbfemale_CheckedChanged(object sender, EventArgs e)
        {
            if (rbfemale.Checked)
            { txtgender.Text = rbfemale.Text; }
            else if (rbmale.Checked)
            { txtgender.Text = rbmale.Text; }
        }

        private void rbsingle_CheckedChanged(object sender, EventArgs e)
        {
            if (rbsingle.Checked)
            { txtstatus.Text = rbsingle.Text; }
            else if (rbmarried.Checked)
            { txtstatus.Text = rbmarried.Text; }
            else if (rbseparated.Checked)
            { txtstatus.Text = rbseparated.Text; }
            else if (rbdivorced.Checked)
            { txtstatus.Text = rbdivorced.Text; }
        }

        private void rbmarried_CheckedChanged(object sender, EventArgs e)
        {
            if (rbmarried.Checked)
            { txtstatus.Text = rbmarried.Text; }
            else if (rbsingle.Checked)
            { txtstatus.Text = rbsingle.Text; }
            else if (rbseparated.Checked)
            { txtstatus.Text = rbseparated.Text; }
            else if (rbdivorced.Checked)
            { txtstatus.Text = rbdivorced.Text; }
        }

        private void rbdivorced_CheckedChanged(object sender, EventArgs e)
        {
            if (rbseparated.Checked)
            { txtstatus.Text = rbseparated.Text; }
            else if (rbmarried.Checked)
            { txtstatus.Text = rbmarried.Text; }
            else if (rbsingle.Checked)
            { txtstatus.Text = rbsingle.Text; }
            else if (rbdivorced.Checked)
            { txtstatus.Text = rbdivorced.Text; }
        }

        private void rbseparated_CheckedChanged(object sender, EventArgs e)
        {
            if (rbafrican.Checked)
            { txtethnicity.Text = rbafrican.Text; }
            else if (rbasian.Checked)
            { txtethnicity.Text = rbasian.Text; }
            else if (rbhispanic.Checked)
            { txtethnicity.Text = rbhispanic.Text; }
            else if (rbwhite.Checked)
            { txtethnicity.Text = rbwhite.Text; }
            else if (rbalaskan.Checked)
            { txtethnicity.Text = rbalaskan.Text; }
        }

        private void rbasian_CheckedChanged(object sender, EventArgs e)
        {
            if (rbasian.Checked)
            { txtethnicity.Text = rbasian.Text; }
            else if (rbafrican.Checked)
            { txtethnicity.Text = rbafrican.Text; }
            else if (rbhispanic.Checked)
            { txtethnicity.Text = rbhispanic.Text; }
            else if (rbwhite.Checked)
            { txtethnicity.Text = rbwhite.Text; }
            else if (rbalaskan.Checked)
            { txtethnicity.Text = rbalaskan.Text; }
        }

        private void rbhispanic_CheckedChanged(object sender, EventArgs e)
        {
            if (rbhispanic.Checked)
            { txtethnicity.Text = rbhispanic.Text; }
            else if (rbasian.Checked)
            { txtethnicity.Text = rbasian.Text; }
            else if (rbafrican.Checked)
            { txtethnicity.Text = rbafrican.Text; }
            else if (rbwhite.Checked)
            { txtethnicity.Text = rbwhite.Text; }
            else if (rbalaskan.Checked)
            { txtethnicity.Text = rbalaskan.Text; }
        }

        private void rbwhite_CheckedChanged(object sender, EventArgs e)
        {
            if (rbwhite.Checked)
            { txtethnicity.Text = rbwhite.Text; }
            else if (rbasian.Checked)
            { txtethnicity.Text = rbasian.Text; }
            else if (rbhispanic.Checked)
            { txtethnicity.Text = rbhispanic.Text; }
            else if (rbafrican.Checked)
            { txtethnicity.Text = rbafrican.Text; }
            else if (rbalaskan.Checked)
            { txtethnicity.Text = rbalaskan.Text; }
        }

        private void rbalaskan_CheckedChanged(object sender, EventArgs e)
        {
            if (rbalaskan.Checked)
            { txtethnicity.Text = rbalaskan.Text; }
            else if (rbasian.Checked)
            { txtethnicity.Text = rbasian.Text; }
            else if (rbhispanic.Checked)
            { txtethnicity.Text = rbhispanic.Text; }
            else if (rbwhite.Checked)
            { txtethnicity.Text = rbwhite.Text; }
            else if (rbafrican.Checked)
            { txtethnicity.Text = rbafrican.Text; }
        }

        private void rbafrican_CheckedChanged(object sender, EventArgs e)
        {
            if (rbafrican.Checked)
            { txtethnicity.Text = rbafrican.Text; }
            else if (rbasian.Checked)
            { txtethnicity.Text = rbasian.Text; }
            else if (rbhispanic.Checked)
            { txtethnicity.Text = rbhispanic.Text; }
            else if (rbwhite.Checked)
            { txtethnicity.Text = rbwhite.Text; }
            else if (rbalaskan.Checked)
            { txtethnicity.Text = rbalaskan.Text; }
        }

        private void btnEditEmployeeInfo_Click(object sender, EventArgs e)
        {
            txtemplastname.Enabled = true;
            txtempfirstname.Enabled = true;
            txtempmidname.Enabled = true;
            txthomeaddress.Enabled = true;
            txtcontactno.Enabled = true;
            txtemailaddress.Enabled = true;
            txtskypename.Enabled = true;
            txtfbacct.Enabled = true;
            txtssno.Enabled = true;
            txtphno.Enabled = true;
            txttin.Enabled = true;
            txtpagibigno.Enabled = true;
            txtposition.Enabled = true;
            txtworklocation.Enabled = true;
            txtphone.Enabled = true;
            txtcontactname.Enabled = true;
            txtcontactaddress.Enabled = true;
            txtpriphone.Enabled = true;
            txtalterphone.Enabled = true;
            txtrelationship.Enabled = true;
            cmbempbirthday.Enabled = true;
            cmbempbirthmonth.Enabled = true;
            cmbempbirthyear.Enabled = true;
        }

        private void txtcontactno_TextChanged(object sender, EventArgs e)
        {

        }


    }
}
