﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Invoice
{
    public partial class InvoiceMDI : Form
    {
        private int childFormNumber = 0;

        public InvoiceMDI()
        {
            InitializeComponent();
        }

    //    AddClient frmaddclient = new AddClient();
        //ViewUpdateClient frmupdateclient = new ViewUpdateClient();
        //CreateInvoice frmcreateinvoice = new CreateInvoice();
        //ViewUpdateInvoice frmupdateinvoice = new ViewUpdateInvoice();

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
           // toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void InvoiceMDI_Load(object sender, EventArgs e)
        {

        }

        private void closeAllChildForm()
        {
            foreach (Form frm in this.MdiChildren)
            {
                frm.Close();
            }
        }

        public void addClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.closeAllChildForm();
            AddClient client = new AddClient();

            client.MdiParent = this;
            client.Show();
            //frmaddclient.MdiParent = this;
            //frmaddclient.Show();
            //frmcreateinvoice.Hide();
            //frmupdateclient.Hide();
            //frmupdateinvoice.Hide();
            
        }

        public void viewUpdateClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.closeAllChildForm();
            ViewUpdateClient client = new ViewUpdateClient();

            client.MdiParent = this;
            client.Show();
           // frmupdateclient.MdiParent = this;
           // frmupdateclient.Show();
           //// frmaddclient.Hide();
           // frmcreateinvoice.Hide();
           // frmupdateinvoice.Hide();
            
            
        }

        public void createInvoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.closeAllChildForm();
            CreateInvoice client = new CreateInvoice();

            client.MdiParent = this;
            client.Show();
          //  frmcreateinvoice.MdiParent = this;
          //  frmcreateinvoice.Show();
          ////  frmaddclient.Hide();
          //  frmupdateclient.Hide();
          //  frmupdateinvoice.Hide();
            
        }

        public void viewUpdateInvoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.closeAllChildForm();
            ViewUpdateInvoice client = new ViewUpdateInvoice();

            client.MdiParent = this;
            client.Show();
          //  frmupdateinvoice.MdiParent = this;
          //  frmupdateinvoice.Show();
          ////  frmaddclient.Hide();
          //  frmcreateinvoice.Hide();
          //  frmupdateclient.Hide();
            
        }

        private void addEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.closeAllChildForm();
            AddEmployee employee = new AddEmployee();

            employee.MdiParent = this;
            employee.Show();
            
        }

        
        private void viewUpdateEmployeeInfoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.closeAllChildForm();
            ViewUpdateEmployeeInfo employee = new ViewUpdateEmployeeInfo();

            employee.MdiParent = this;
            employee.Show();
        }

        

        
    }
}
