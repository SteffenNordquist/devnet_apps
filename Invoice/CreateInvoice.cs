﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text;
using System.Diagnostics;


namespace Invoice
{
    public partial class CreateInvoice : Form
    {
        public CreateInvoice()
        {
            InitializeComponent();
        }

        
        public static MongoCollection<ClientEntity> Client = new MongoClient(@"mongodb://client144:client144devnetworks@144.76.166.207/DevNet").GetServer().GetDatabase("DevNet").GetCollection<ClientEntity>("Clients");
        public static MongoCollection<InvoiceEntity> Invoices = new MongoClient(@"mongodb://client144:client144devnetworks@144.76.166.207/DevNet").GetServer().GetDatabase("DevNet").GetCollection<InvoiceEntity>("Invoices");

        
        private void CreateInvoice_Load(object sender, EventArgs e)
        {
            Combofill();
         
            int invoicescount = Convert.ToInt32(Invoices.Count());
            string text = invoicescount.ToString("0");
            int text2 = Convert.ToInt32(text);
            text2++;
            string text3 = Convert.ToString(text2);
            if (text3.Length <= 1)
            {
                txtInvoicenum.Text = ("INV-" + "00000" + text3);
            }
            else if (text3.Length <= 2)
            {
                txtInvoicenum.Text = ("INV-" + "0000" + text3);
            }
            else if (text3.Length <= 3)
            {
                txtInvoicenum.Text = ("INV-" + "000" + text3);
            }
            else if (text3.Length <= 4)
            {
                txtInvoicenum.Text = ("INV-" + "00" + text3);
            }
            else if (text3.Length <= 4)
            {
                txtInvoicenum.Text = ("INV-" + "0" + text3);
            }
            
            


            txtdatenow.Text = DateTime.Now.ToShortDateString();

            DateTime date = new DateTime();
            DateTime duedate = date.AddDays(15);
            txtdatedue.Text = duedate.ToShortDateString();

            
           
        }

        private void Combofill()
        {

            var clients = Client.FindAll();

            foreach (var clientname in clients) {

                comboBoxclients.Items.Add(clientname.clientAcctName);
            
            }

        }

        private void comboBoxclients_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = comboBoxclients.Text;


            var clientinfo = Client.FindOne(Query.EQ("clientAcctName", selected));

            txtclientname.Text = clientinfo.clientAcctName;
            txtstreet.Text = clientinfo.clientStreet;
            txtcity.Text = clientinfo.clientCity;
            txtcountry.Text = clientinfo.clientCountry;
            txtclientrep.Text = clientinfo.clientRep;
            txtclientacctno.Text = clientinfo.clientId;  
            
        }

        private void btnCreateInvoice_Click(object sender, EventArgs e)
        {

            if (txtInvoicenum.Text != "" &&
                txtdescription.Text != "" &&
                txtamount.Text != "" &&
                txtclientacctno.Text != "" &&
                txtclientname.Text != "" &&
                txtstreet.Text != "" &&
                txtclientrep.Text != "")
            {
                InvoiceEntity invoice = new InvoiceEntity
                {
                    invoiceNumber = txtInvoicenum.Text,
                    datenow = Convert.ToDateTime(txtdatenow.Text),
                    datedue = Convert.ToDateTime(txtdatedue.Text),
                    description = txtdescription.Text,
                    amount = Convert.ToDouble(txtamount.Text),
                    note = txtnote.Text,
                    vat = lblvat.Text,
                    totalamt = Convert.ToDouble(txttotalamt.Text),

                    clientId = txtclientacctno.Text,
                    clientAcctName = txtclientname.Text,
                    clientStreet = txtstreet.Text,
                    clientCity = txtcity.Text,
                    clientCountry = txtcountry.Text,
                    clientRep = txtclientrep.Text
                };

                Invoices.Insert(invoice);
                MessageBox.Show("Successfully Saved!");
                btnGenerate.Enabled = true;
                btnCreateInvoice.Enabled = false;
            }
            else
            {
                MessageBox.Show("Please fill-in necessary details!");
            }    

        }

        private void txtclientname_TextChanged(object sender, EventArgs e)
        {

        }

        /*************Genrate PDF***************/  
       
        private void btnGenerate_Click(object sender, EventArgs e)
        {
            GeneratePDF.GeneratePdf("", txtclientname.Text + ".pdf",
                                        txtclientacctno.Text, txtclientname.Text, txtstreet.Text, txtcity.Text, txtcountry.Text, txtclientrep.Text, txtInvoicenum.Text, txtdatenow.Text,
                                        txtdatedue.Text, txtdescription.Text, txtnote.Text, txtamount.Text, lblvat.Text, txttotalamt.Text); 
        }

        private void txtamount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.txtamount.Text.Length > 3)
                {
                    string text = this.txtamount.Text.Replace(",", "");
                    double n = Convert.ToDouble(text);
                    this.txtamount.Text = string.Format("{0:0,0.00}", n);
                    this.txtamount.SelectionStart = this.txtamount.Text.Length;
                }
            }
            catch
            {
                MessageBox.Show("Invalid Input", "Create Invoice");
            }
           
            
                txttotalamt.Text = txtamount.Text;
                    
                
                if (vatno.Checked == true)
                {
                    txttotalamt.Text = txtamount.Text;
                }
            
        }

        private void vatyes_CheckedChanged(object sender, EventArgs e)
        {
            if (vatyes.Checked == true)
            {
                double vat = Convert.ToDouble(txtamount.Text) * 0.12;
                double totalwithvat = vat + Convert.ToDouble(txtamount.Text);
                txttotalamt.Text = totalwithvat.ToString();

                lblvat.Text = "12%";
            }
            if (vatno.Checked == true)
            {
                txttotalamt.Text = txtamount.Text;
                lblvat.Text = "0%";
            }
        }

        private void txttotalamt_TextChanged(object sender, EventArgs e)
        {
            try
            {

                if (this.txttotalamt.Text.Length > 3)
                {
                    string text = this.txttotalamt.Text.Replace(",", "");
                    double n = Convert.ToDouble(text);
                    this.txttotalamt.Text = string.Format("{0:0,0.00}", n);
                    this.txttotalamt.SelectionStart = this.txttotalamt.Text.Length;
                }
            }
            catch
            {
                MessageBox.Show("Invalid Input", "Create Invoice");
            }
        }

        private void vatno_CheckedChanged(object sender, EventArgs e)
        {
            if (vatno.Checked == true)
            {
                txttotalamt.Text = txtamount.Text;
                lblvat.Text = "0%";
            }
            if (vatyes.Checked == true)
            {
                double vat = Convert.ToDouble(txtamount.Text) * 0.12;
                double totalwithvat = vat + Convert.ToDouble(txtamount.Text);
                txttotalamt.Text = totalwithvat.ToString();
                lblvat.Text = "12%";
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void dt2_ValueChanged(object sender, EventArgs e)
        {
            txtdatenow.Text = dt2.Value.ToShortDateString();
        }

        private void txtdatenow_TextChanged(object sender, EventArgs e)
        {
            DateTime duedate = dt2.Value.AddDays(15);
            txtdatedue.Text = duedate.ToShortDateString();
        }

        private void txtInvoicenum_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
