﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Invoice
{
    public partial class ViewUpdateInvoice : Form
    {
        public ViewUpdateInvoice()
        {
            InitializeComponent();
        }

        public static MongoCollection<InvoiceEntity> Invoices = new MongoClient(@"mongodb://client144:client144devnetworks@144.76.166.207/DevNet").GetServer().GetDatabase("DevNet").GetCollection<InvoiceEntity>("Invoices");
        public static MongoCollection<ClientEntity> Client = new MongoClient(@"mongodb://client144:client144devnetworks@144.76.166.207/DevNet").GetServer().GetDatabase("DevNet").GetCollection<ClientEntity>("Clients");


        private void ViewUpdateInvoice_Load(object sender, EventArgs e)
        {

            Combofill();

            var invoice = Invoices.FindAll();

            foreach (var showinvoice in invoice)
            {
                dgviewinvoice.Rows.Add(showinvoice.invoiceNumber,
                                      showinvoice.clientId,
                                      showinvoice.clientAcctName,
                                      showinvoice.clientStreet,
                                      showinvoice.clientCity,
                                      showinvoice.clientCountry,
                                      showinvoice.clientRep,
                                      showinvoice.description,
                                      showinvoice.note,
                                      showinvoice.datenow,
                                      showinvoice.datedue,
                                      showinvoice.amount,
                                      showinvoice.vat,
                                      showinvoice.totalamt);
            }
        }

        private void dgviewinvoice_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex >= 0)
            {
                
                DataGridViewRow row = this.dgviewinvoice.Rows[e.RowIndex];
                

                txtInvoicenum.Text = row.Cells["invoicenum"].Value.ToString();
                txtclientacctno.Text = row.Cells["clientidno"].Value.ToString();
                txtclientname.Text = row.Cells["clientname"].Value.ToString();
                txtstreet.Text = row.Cells["street"].Value.ToString();
                txtcity.Text = row.Cells["city"].Value.ToString();
                txtcountry.Text = row.Cells["country"].Value.ToString();
                txtclientrep.Text = row.Cells["rep"].Value.ToString();
                txtdescription.Text = row.Cells["desc"].Value.ToString();
                txtnote.Text = row.Cells["note"].Value.ToString();
                txtdatenow.Text = row.Cells["datenow"].Value.ToString();
                txtdatedue.Text = row.Cells["datedue"].Value.ToString();
                txtamount.Text = row.Cells["amt"].Value.ToString();
                lblvat.Text = row.Cells["vat"].Value.ToString();
                txttotalamt.Text = row.Cells["totalamt"].Value.ToString();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            txtdatenow.Enabled = true;
            comboBoxclients.Enabled = true;
            txtdescription.Enabled = true;
            txtnote.Enabled = true;
            dt2.Enabled = true;
            txtamount.Enabled = true;
            vatyes.Enabled = true;
            vatno.Enabled = true;
            btnSaveInvoice.Enabled = true;

        }

        private void txtamount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.txtamount.Text.Length > 3)
                {
                    string text = this.txtamount.Text.Replace(",", "");
                    double n = Convert.ToDouble(text);
                    this.txtamount.Text = string.Format("{0:0,0.00}", n);
                    this.txtamount.SelectionStart = this.txtamount.Text.Length;
                }

                txttotalamt.Text = txtamount.Text;


                if (vatno.Checked == true)
                {
                    txttotalamt.Text = txtamount.Text;
                }
            }
            catch
            {
                MessageBox.Show("Invalid Input", "Create Invoice");
            }
        }


        private void btnSaveInvoice_Click(object sender, EventArgs e)
        {
           
        }

        private void Combofill()
        {

            var clients = Client.FindAll();

            foreach (var clientname in clients)
            {

                comboBoxclients.Items.Add(clientname.clientAcctName);

            }
        }


        private void comboBoxclients_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = comboBoxclients.Text;


            var clientinfo = Client.FindOne(Query.EQ("clientAcctName", selected));

            txtclientname.Text = clientinfo.clientAcctName;
            txtstreet.Text = clientinfo.clientStreet;
            txtcity.Text = clientinfo.clientCity;
            txtcountry.Text = clientinfo.clientCountry;
            txtclientrep.Text = clientinfo.clientRep;
            txtclientacctno.Text = clientinfo.clientId;
        }

        private void dt2_ValueChanged(object sender, EventArgs e)
        {
            txtdatenow.Text = dt2.Value.ToShortDateString();

        }

        private void txtdatenow_TextChanged(object sender, EventArgs e)
        {
            DateTime duedate = dt2.Value.AddDays(15);
            txtdatedue.Text = duedate.ToShortDateString();

        }

        private void vatyes_CheckedChanged(object sender, EventArgs e)
        {
            if (vatyes.Checked == true)
            {
                double vat = Convert.ToDouble(txtamount.Text) * 0.12;
                double totalwithvat = vat + Convert.ToDouble(txtamount.Text);
                txttotalamt.Text = totalwithvat.ToString();

                lblvat.Text = "12%";
            }
            else if (vatno.Checked == true)
            {
                txttotalamt.Text = txtamount.Text;
                lblvat.Text = "0%";
            }

        }

        private void vatno_CheckedChanged(object sender, EventArgs e)
        {
            if (vatno.Checked == true)
            {
                txttotalamt.Text = txtamount.Text;
                lblvat.Text = "0%";
            }
            else if (vatyes.Checked == true)
            {
                double vat = Convert.ToDouble(txtamount.Text) * 0.12;
                double totalwithvat = vat + Convert.ToDouble(txtamount.Text);
                txttotalamt.Text = totalwithvat.ToString();
                lblvat.Text = "12%";
            }

        }

        private void txttotalamt_TextChanged(object sender, EventArgs e)
        {
            if (this.txttotalamt.Text.Length > 3)
            {
                string text = this.txttotalamt.Text.Replace(",", "");
                double n = Convert.ToDouble(text);
                this.txttotalamt.Text = string.Format("{0:0,0.00}", n);
                this.txttotalamt.SelectionStart = this.txttotalamt.Text.Length;

            }

        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            GeneratePDF.GeneratePdf("", txtclientname.Text + ".pdf",
                                        txtclientacctno.Text, txtclientname.Text, txtstreet.Text, txtcity.Text, txtcountry.Text, txtclientrep.Text, txtInvoicenum.Text, txtdatenow.Text,
                                        txtdatedue.Text, txtdescription.Text, txtnote.Text, txtamount.Text, lblvat.Text, txttotalamt.Text);
        }

        private void btnSaveInvoice_Click_1(object sender, EventArgs e)
        {
            var update = MongoDB.Driver.Builders.Update
                                             .Set("clientId", txtclientacctno.Text)
                                             .Set("clientAcctName", txtclientname.Text)
                                             .Set("clientStreet", txtstreet.Text)
                                             .Set("clientCity", txtcity.Text)
                                             .Set("clientCountry", txtcountry.Text)
                                             .Set("clientRep", txtclientrep.Text)
                                             .Set("description", txtdescription.Text)
                                             .Set("note", txtnote.Text)
                                             .Set("amount", Convert.ToDouble(txtamount.Text))
                                             .Set("vat", lblvat.Text)
                                             .Set("totalamt", Convert.ToDouble(txttotalamt.Text));

            Invoices.Update(Query.EQ("_id", txtInvoicenum.Text), update);

            MessageBox.Show("Successfully Updated!", "Update Invoice");

            btnGenerate.Enabled = true;
        }







    }
}

