﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invoice
{
    public class EmployeeEntity
    {
        [BsonId]
        public string empid { get; set; }
        public string empfirstname { get; set; }
        public string empmiddlename { get; set; }
        public string emplastname { get; set; }
        public string empbirthmonth { get; set; }
        public int empbirthday { get; set; }
        public int empbirthyear { get; set; }
        public string emphomeaddress { get; set; }
        public string empcontactno { get; set; }
        public string empemailaddress { get; set; }
        public string empskypename { get; set; }
        public string empfbacct { get; set; }
        public string empSSSNo { get; set; }
        public string empPhilHealthNo { get; set; }
        public string empTIN { get; set; }
        public string empPagIbigNo { get; set; }
        public string empgender { get; set; }
        public string empstatus { get; set; }
        public string empethnicity { get; set; }
        public string empposition { get; set; }
        public DateTime empstartdate { get; set; }
        public string empworklocation { get; set; }
        public string empworkphone { get; set; }
        public string contactname { get; set; }
        public string contactaddress { get; set; }
        public string primaryphone { get; set; }
        public string alterphone { get; set; }
        public string relationship { get; set; }
        
    }
}
