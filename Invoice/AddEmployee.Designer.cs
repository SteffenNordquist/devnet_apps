﻿namespace Invoice
{
    partial class AddEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtpagibigno = new System.Windows.Forms.TextBox();
            this.txtphno = new System.Windows.Forms.TextBox();
            this.txttin = new System.Windows.Forms.TextBox();
            this.txtssno = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtfbacct = new System.Windows.Forms.TextBox();
            this.txtskypename = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtemailaddress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txthomeaddress = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtcontactno = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbempbirthday = new System.Windows.Forms.ComboBox();
            this.cmbempbirthyear = new System.Windows.Forms.ComboBox();
            this.cmbempbirthmonth = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtempmidname = new System.Windows.Forms.TextBox();
            this.txtemplastname = new System.Windows.Forms.TextBox();
            this.txtempfirstname = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbfemale = new System.Windows.Forms.RadioButton();
            this.rbmale = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.txtgender = new System.Windows.Forms.Label();
            this.rbsingle = new System.Windows.Forms.RadioButton();
            this.rbalaskan = new System.Windows.Forms.RadioButton();
            this.rbwhite = new System.Windows.Forms.RadioButton();
            this.rbseparated = new System.Windows.Forms.RadioButton();
            this.rbhispanic = new System.Windows.Forms.RadioButton();
            this.rbmarried = new System.Windows.Forms.RadioButton();
            this.rbdivorced = new System.Windows.Forms.RadioButton();
            this.rbafrican = new System.Windows.Forms.RadioButton();
            this.rbasian = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtempid = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dtstartdate = new System.Windows.Forms.DateTimePicker();
            this.txtstartdate = new System.Windows.Forms.TextBox();
            this.txtworklocation = new System.Windows.Forms.TextBox();
            this.txtphone = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtposition = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtrelationship = new System.Windows.Forms.TextBox();
            this.txtalterphone = new System.Windows.Forms.TextBox();
            this.txtpriphone = new System.Windows.Forms.TextBox();
            this.txtcontactaddress = new System.Windows.Forms.TextBox();
            this.txtcontactname = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtstatus = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtethnicity = new System.Windows.Forms.Label();
            this.btnSaveEmpInfo = new System.Windows.Forms.Button();
            this.txtstartdatesuffix = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox1.Controls.Add(this.txtpagibigno);
            this.groupBox1.Controls.Add(this.txtphno);
            this.groupBox1.Controls.Add(this.txttin);
            this.groupBox1.Controls.Add(this.txtssno);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtfbacct);
            this.groupBox1.Controls.Add(this.txtskypename);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtemailaddress);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txthomeaddress);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtcontactno);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cmbempbirthday);
            this.groupBox1.Controls.Add(this.cmbempbirthyear);
            this.groupBox1.Controls.Add(this.cmbempbirthmonth);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtempmidname);
            this.groupBox1.Controls.Add(this.txtemplastname);
            this.groupBox1.Controls.Add(this.txtempfirstname);
            this.groupBox1.Location = new System.Drawing.Point(33, 43);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.groupBox1.Size = new System.Drawing.Size(718, 377);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PERSONAL INFORMATION";
            // 
            // txtpagibigno
            // 
            this.txtpagibigno.Location = new System.Drawing.Point(521, 341);
            this.txtpagibigno.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtpagibigno.Name = "txtpagibigno";
            this.txtpagibigno.Size = new System.Drawing.Size(185, 27);
            this.txtpagibigno.TabIndex = 33;
            // 
            // txtphno
            // 
            this.txtphno.Location = new System.Drawing.Point(521, 265);
            this.txtphno.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtphno.Name = "txtphno";
            this.txtphno.Size = new System.Drawing.Size(185, 27);
            this.txtphno.TabIndex = 32;
            // 
            // txttin
            // 
            this.txttin.Location = new System.Drawing.Point(521, 303);
            this.txttin.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txttin.Name = "txttin";
            this.txttin.Size = new System.Drawing.Size(185, 27);
            this.txttin.TabIndex = 31;
            // 
            // txtssno
            // 
            this.txtssno.Location = new System.Drawing.Point(521, 227);
            this.txtssno.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtssno.Name = "txtssno";
            this.txtssno.Size = new System.Drawing.Size(185, 27);
            this.txtssno.TabIndex = 30;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(335, 351);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 19);
            this.label11.TabIndex = 29;
            this.label11.Text = "Pag-ibig No:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(335, 313);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(190, 19);
            this.label10.TabIndex = 28;
            this.label10.Text = "Tax Identification No.(TIN):";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(335, 275);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(112, 19);
            this.label9.TabIndex = 27;
            this.label9.Text = "Phil Health No:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(331, 237);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(175, 19);
            this.label8.TabIndex = 26;
            this.label8.Text = "Social Security No.(SSS):";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 348);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(112, 19);
            this.label7.TabIndex = 25;
            this.label7.Text = "Facebook Acct:";
            // 
            // txtfbacct
            // 
            this.txtfbacct.Location = new System.Drawing.Point(147, 341);
            this.txtfbacct.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtfbacct.Name = "txtfbacct";
            this.txtfbacct.Size = new System.Drawing.Size(185, 27);
            this.txtfbacct.TabIndex = 24;
            // 
            // txtskypename
            // 
            this.txtskypename.Location = new System.Drawing.Point(149, 303);
            this.txtskypename.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtskypename.Name = "txtskypename";
            this.txtskypename.Size = new System.Drawing.Size(185, 27);
            this.txtskypename.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 310);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 19);
            this.label6.TabIndex = 22;
            this.label6.Text = "Skype Acct Name:";
            // 
            // txtemailaddress
            // 
            this.txtemailaddress.Location = new System.Drawing.Point(147, 265);
            this.txtemailaddress.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtemailaddress.Name = "txtemailaddress";
            this.txtemailaddress.Size = new System.Drawing.Size(185, 27);
            this.txtemailaddress.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 272);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 19);
            this.label4.TabIndex = 20;
            this.label4.Text = "Email Address:";
            // 
            // txthomeaddress
            // 
            this.txthomeaddress.Location = new System.Drawing.Point(147, 138);
            this.txthomeaddress.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txthomeaddress.Multiline = true;
            this.txthomeaddress.Name = "txthomeaddress";
            this.txthomeaddress.Size = new System.Drawing.Size(559, 62);
            this.txthomeaddress.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 133);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 19);
            this.label3.TabIndex = 18;
            this.label3.Text = "Home Address:";
            // 
            // txtcontactno
            // 
            this.txtcontactno.Location = new System.Drawing.Point(149, 227);
            this.txtcontactno.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtcontactno.Name = "txtcontactno";
            this.txtcontactno.Size = new System.Drawing.Size(185, 27);
            this.txtcontactno.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 234);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 19);
            this.label2.TabIndex = 16;
            this.label2.Text = "Contact Number/s:";
            // 
            // cmbempbirthday
            // 
            this.cmbempbirthday.FormattingEnabled = true;
            this.cmbempbirthday.Location = new System.Drawing.Point(265, 81);
            this.cmbempbirthday.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.cmbempbirthday.Name = "cmbempbirthday";
            this.cmbempbirthday.Size = new System.Drawing.Size(68, 27);
            this.cmbempbirthday.TabIndex = 15;
            this.cmbempbirthday.Text = "Day";
            this.cmbempbirthday.SelectedIndexChanged += new System.EventHandler(this.cmbempbirthday_SelectedIndexChanged);
            // 
            // cmbempbirthyear
            // 
            this.cmbempbirthyear.FormattingEnabled = true;
            this.cmbempbirthyear.Location = new System.Drawing.Point(342, 81);
            this.cmbempbirthyear.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.cmbempbirthyear.Name = "cmbempbirthyear";
            this.cmbempbirthyear.Size = new System.Drawing.Size(63, 27);
            this.cmbempbirthyear.TabIndex = 14;
            this.cmbempbirthyear.Text = "Year";
            this.cmbempbirthyear.SelectedIndexChanged += new System.EventHandler(this.cmbempbirthyear_SelectedIndexChanged);
            // 
            // cmbempbirthmonth
            // 
            this.cmbempbirthmonth.FormattingEnabled = true;
            this.cmbempbirthmonth.Location = new System.Drawing.Point(149, 81);
            this.cmbempbirthmonth.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.cmbempbirthmonth.Name = "cmbempbirthmonth";
            this.cmbempbirthmonth.Size = new System.Drawing.Size(107, 27);
            this.cmbempbirthmonth.TabIndex = 13;
            this.cmbempbirthmonth.Text = "Month";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 82);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 19);
            this.label5.TabIndex = 7;
            this.label5.Text = "Date of Birth:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "Employee Name:";
            // 
            // txtempmidname
            // 
            this.txtempmidname.Location = new System.Drawing.Point(550, 27);
            this.txtempmidname.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtempmidname.Name = "txtempmidname";
            this.txtempmidname.Size = new System.Drawing.Size(156, 27);
            this.txtempmidname.TabIndex = 2;
            this.txtempmidname.Text = "Middle Name";
            this.txtempmidname.Click += new System.EventHandler(this.txtempmidname_Click);
            // 
            // txtemplastname
            // 
            this.txtemplastname.Location = new System.Drawing.Point(149, 27);
            this.txtemplastname.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtemplastname.Name = "txtemplastname";
            this.txtemplastname.Size = new System.Drawing.Size(191, 27);
            this.txtemplastname.TabIndex = 1;
            this.txtemplastname.Text = "Last Name";
            this.txtemplastname.Click += new System.EventHandler(this.txtemplastname_Click);
            // 
            // txtempfirstname
            // 
            this.txtempfirstname.Location = new System.Drawing.Point(353, 27);
            this.txtempfirstname.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtempfirstname.Name = "txtempfirstname";
            this.txtempfirstname.Size = new System.Drawing.Size(185, 27);
            this.txtempfirstname.TabIndex = 0;
            this.txtempfirstname.Text = "First Name";
            this.txtempfirstname.Click += new System.EventHandler(this.txtempfirstname_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox2.Controls.Add(this.rbfemale);
            this.groupBox2.Controls.Add(this.rbmale);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtgender);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox2.Location = new System.Drawing.Point(33, 428);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.groupBox2.Size = new System.Drawing.Size(134, 168);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // rbfemale
            // 
            this.rbfemale.AutoSize = true;
            this.rbfemale.Location = new System.Drawing.Point(15, 70);
            this.rbfemale.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.rbfemale.Name = "rbfemale";
            this.rbfemale.Size = new System.Drawing.Size(75, 23);
            this.rbfemale.TabIndex = 39;
            this.rbfemale.TabStop = true;
            this.rbfemale.Text = "Female";
            this.rbfemale.UseVisualStyleBackColor = true;
            this.rbfemale.CheckedChanged += new System.EventHandler(this.rbfemale_CheckedChanged);
            // 
            // rbmale
            // 
            this.rbmale.AutoSize = true;
            this.rbmale.Location = new System.Drawing.Point(14, 48);
            this.rbmale.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.rbmale.Name = "rbmale";
            this.rbmale.Size = new System.Drawing.Size(61, 23);
            this.rbmale.TabIndex = 29;
            this.rbmale.TabStop = true;
            this.rbmale.Text = "Male";
            this.rbmale.UseVisualStyleBackColor = true;
            this.rbmale.CheckedChanged += new System.EventHandler(this.rbmale_CheckedChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 23);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 19);
            this.label14.TabIndex = 28;
            this.label14.Text = "Gender:";
            // 
            // txtgender
            // 
            this.txtgender.AutoSize = true;
            this.txtgender.Location = new System.Drawing.Point(34, 72);
            this.txtgender.Name = "txtgender";
            this.txtgender.Size = new System.Drawing.Size(55, 19);
            this.txtgender.TabIndex = 40;
            this.txtgender.Text = "female";
            // 
            // rbsingle
            // 
            this.rbsingle.AutoSize = true;
            this.rbsingle.Location = new System.Drawing.Point(26, 48);
            this.rbsingle.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.rbsingle.Name = "rbsingle";
            this.rbsingle.Size = new System.Drawing.Size(68, 23);
            this.rbsingle.TabIndex = 38;
            this.rbsingle.TabStop = true;
            this.rbsingle.Text = "Single";
            this.rbsingle.UseVisualStyleBackColor = true;
            this.rbsingle.CheckedChanged += new System.EventHandler(this.rbsingle_CheckedChanged);
            // 
            // rbalaskan
            // 
            this.rbalaskan.AutoSize = true;
            this.rbalaskan.Location = new System.Drawing.Point(21, 134);
            this.rbalaskan.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.rbalaskan.Name = "rbalaskan";
            this.rbalaskan.Size = new System.Drawing.Size(256, 23);
            this.rbalaskan.TabIndex = 37;
            this.rbalaskan.TabStop = true;
            this.rbalaskan.Text = "Alaskan Native / Native American";
            this.rbalaskan.UseVisualStyleBackColor = true;
            this.rbalaskan.CheckedChanged += new System.EventHandler(this.rbalaskan_CheckedChanged);
            // 
            // rbwhite
            // 
            this.rbwhite.AutoSize = true;
            this.rbwhite.Location = new System.Drawing.Point(21, 113);
            this.rbwhite.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.rbwhite.Name = "rbwhite";
            this.rbwhite.Size = new System.Drawing.Size(183, 23);
            this.rbwhite.TabIndex = 36;
            this.rbwhite.TabStop = true;
            this.rbwhite.Text = "White / Non - Hispanic";
            this.rbwhite.UseVisualStyleBackColor = true;
            this.rbwhite.CheckedChanged += new System.EventHandler(this.rbwhite_CheckedChanged);
            // 
            // rbseparated
            // 
            this.rbseparated.AutoSize = true;
            this.rbseparated.Location = new System.Drawing.Point(26, 109);
            this.rbseparated.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.rbseparated.Name = "rbseparated";
            this.rbseparated.Size = new System.Drawing.Size(97, 23);
            this.rbseparated.TabIndex = 35;
            this.rbseparated.TabStop = true;
            this.rbseparated.Text = "Separated";
            this.rbseparated.UseVisualStyleBackColor = true;
            this.rbseparated.CheckedChanged += new System.EventHandler(this.rbseparated_CheckedChanged);
            // 
            // rbhispanic
            // 
            this.rbhispanic.AutoSize = true;
            this.rbhispanic.Location = new System.Drawing.Point(21, 92);
            this.rbhispanic.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.rbhispanic.Name = "rbhispanic";
            this.rbhispanic.Size = new System.Drawing.Size(84, 23);
            this.rbhispanic.TabIndex = 34;
            this.rbhispanic.TabStop = true;
            this.rbhispanic.Text = "Hispanic";
            this.rbhispanic.UseVisualStyleBackColor = true;
            this.rbhispanic.CheckedChanged += new System.EventHandler(this.rbhispanic_CheckedChanged);
            // 
            // rbmarried
            // 
            this.rbmarried.AutoSize = true;
            this.rbmarried.Location = new System.Drawing.Point(26, 68);
            this.rbmarried.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.rbmarried.Name = "rbmarried";
            this.rbmarried.Size = new System.Drawing.Size(82, 23);
            this.rbmarried.TabIndex = 33;
            this.rbmarried.TabStop = true;
            this.rbmarried.Text = "Married";
            this.rbmarried.UseVisualStyleBackColor = true;
            this.rbmarried.CheckedChanged += new System.EventHandler(this.rbmarried_CheckedChanged);
            // 
            // rbdivorced
            // 
            this.rbdivorced.AutoSize = true;
            this.rbdivorced.Location = new System.Drawing.Point(26, 89);
            this.rbdivorced.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.rbdivorced.Name = "rbdivorced";
            this.rbdivorced.Size = new System.Drawing.Size(88, 23);
            this.rbdivorced.TabIndex = 32;
            this.rbdivorced.TabStop = true;
            this.rbdivorced.Text = "Divorced";
            this.rbdivorced.UseVisualStyleBackColor = true;
            this.rbdivorced.CheckedChanged += new System.EventHandler(this.rbdivorced_CheckedChanged);
            // 
            // rbafrican
            // 
            this.rbafrican.AutoSize = true;
            this.rbafrican.Location = new System.Drawing.Point(21, 52);
            this.rbafrican.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.rbafrican.Name = "rbafrican";
            this.rbafrican.Size = new System.Drawing.Size(154, 23);
            this.rbafrican.TabIndex = 31;
            this.rbafrican.TabStop = true;
            this.rbafrican.Text = "African - American";
            this.rbafrican.UseVisualStyleBackColor = true;
            this.rbafrican.CheckedChanged += new System.EventHandler(this.rbafrican_CheckedChanged);
            // 
            // rbasian
            // 
            this.rbasian.AutoSize = true;
            this.rbasian.Location = new System.Drawing.Point(21, 72);
            this.rbasian.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.rbasian.Name = "rbasian";
            this.rbasian.Size = new System.Drawing.Size(181, 23);
            this.rbasian.TabIndex = 30;
            this.rbasian.TabStop = true;
            this.rbasian.Text = "Asian / Pacific Islander";
            this.rbasian.UseVisualStyleBackColor = true;
            this.rbasian.CheckedChanged += new System.EventHandler(this.rbasian_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(22, 23);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(110, 19);
            this.label13.TabIndex = 27;
            this.label13.Text = "Marital Status:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 27);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 19);
            this.label12.TabIndex = 26;
            this.label12.Text = "Ethnicity:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(29, 13);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(122, 19);
            this.label15.TabIndex = 4;
            this.label15.Text = "Employee ID No.";
            // 
            // txtempid
            // 
            this.txtempid.Enabled = false;
            this.txtempid.Location = new System.Drawing.Point(222, 8);
            this.txtempid.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtempid.Name = "txtempid";
            this.txtempid.Size = new System.Drawing.Size(53, 27);
            this.txtempid.TabIndex = 34;
            this.txtempid.TextChanged += new System.EventHandler(this.txtempid_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox3.Controls.Add(this.dtstartdate);
            this.groupBox3.Controls.Add(this.txtstartdate);
            this.groupBox3.Controls.Add(this.txtworklocation);
            this.groupBox3.Controls.Add(this.txtphone);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.txtposition);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox3.Location = new System.Drawing.Point(759, 43);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.groupBox3.Size = new System.Drawing.Size(447, 225);
            this.groupBox3.TabIndex = 35;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "JOB INFORMATION";
            // 
            // dtstartdate
            // 
            this.dtstartdate.Location = new System.Drawing.Point(330, 72);
            this.dtstartdate.Name = "dtstartdate";
            this.dtstartdate.Size = new System.Drawing.Size(18, 27);
            this.dtstartdate.TabIndex = 41;
            this.dtstartdate.ValueChanged += new System.EventHandler(this.dtstartdate_ValueChanged_1);
            // 
            // txtstartdate
            // 
            this.txtstartdate.Location = new System.Drawing.Point(137, 72);
            this.txtstartdate.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtstartdate.Name = "txtstartdate";
            this.txtstartdate.Size = new System.Drawing.Size(185, 27);
            this.txtstartdate.TabIndex = 40;
            // 
            // txtworklocation
            // 
            this.txtworklocation.Location = new System.Drawing.Point(137, 110);
            this.txtworklocation.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtworklocation.Multiline = true;
            this.txtworklocation.Name = "txtworklocation";
            this.txtworklocation.Size = new System.Drawing.Size(295, 57);
            this.txtworklocation.TabIndex = 39;
            // 
            // txtphone
            // 
            this.txtphone.Location = new System.Drawing.Point(137, 177);
            this.txtphone.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtphone.Name = "txtphone";
            this.txtphone.Size = new System.Drawing.Size(185, 27);
            this.txtphone.TabIndex = 38;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(13, 187);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(98, 19);
            this.label19.TabIndex = 37;
            this.label19.Text = "Work Phone:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(14, 110);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(112, 19);
            this.label18.TabIndex = 36;
            this.label18.Text = "Work Location:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 80);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(83, 19);
            this.label17.TabIndex = 35;
            this.label17.Text = "Start Date:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(13, 44);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(113, 19);
            this.label16.TabIndex = 34;
            this.label16.Text = "Title / Position:";
            // 
            // txtposition
            // 
            this.txtposition.Location = new System.Drawing.Point(137, 34);
            this.txtposition.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtposition.Name = "txtposition";
            this.txtposition.Size = new System.Drawing.Size(295, 27);
            this.txtposition.TabIndex = 34;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.txtrelationship);
            this.groupBox4.Controls.Add(this.txtalterphone);
            this.groupBox4.Controls.Add(this.txtpriphone);
            this.groupBox4.Controls.Add(this.txtcontactaddress);
            this.groupBox4.Controls.Add(this.txtcontactname);
            this.groupBox4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox4.Location = new System.Drawing.Point(759, 276);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.groupBox4.Size = new System.Drawing.Size(447, 271);
            this.groupBox4.TabIndex = 36;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "EMERGENCY CONTACT";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(11, 43);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(110, 19);
            this.label24.TabIndex = 44;
            this.label24.Text = "Contact Name:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(14, 232);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(97, 19);
            this.label23.TabIndex = 43;
            this.label23.Text = "Relationship:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 81);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(124, 19);
            this.label22.TabIndex = 42;
            this.label22.Text = "Contact Address:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(14, 156);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(115, 19);
            this.label21.TabIndex = 41;
            this.label21.Text = "Primary Phone:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(14, 194);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(126, 19);
            this.label20.TabIndex = 40;
            this.label20.Text = "Alternate Phone:";
            // 
            // txtrelationship
            // 
            this.txtrelationship.Location = new System.Drawing.Point(146, 224);
            this.txtrelationship.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtrelationship.Name = "txtrelationship";
            this.txtrelationship.Size = new System.Drawing.Size(185, 27);
            this.txtrelationship.TabIndex = 39;
            // 
            // txtalterphone
            // 
            this.txtalterphone.Location = new System.Drawing.Point(146, 186);
            this.txtalterphone.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtalterphone.Name = "txtalterphone";
            this.txtalterphone.Size = new System.Drawing.Size(185, 27);
            this.txtalterphone.TabIndex = 38;
            // 
            // txtpriphone
            // 
            this.txtpriphone.Location = new System.Drawing.Point(146, 148);
            this.txtpriphone.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtpriphone.Name = "txtpriphone";
            this.txtpriphone.Size = new System.Drawing.Size(185, 27);
            this.txtpriphone.TabIndex = 37;
            // 
            // txtcontactaddress
            // 
            this.txtcontactaddress.Location = new System.Drawing.Point(146, 79);
            this.txtcontactaddress.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtcontactaddress.Multiline = true;
            this.txtcontactaddress.Name = "txtcontactaddress";
            this.txtcontactaddress.Size = new System.Drawing.Size(286, 58);
            this.txtcontactaddress.TabIndex = 36;
            // 
            // txtcontactname
            // 
            this.txtcontactname.Location = new System.Drawing.Point(146, 46);
            this.txtcontactname.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtcontactname.Name = "txtcontactname";
            this.txtcontactname.Size = new System.Drawing.Size(286, 27);
            this.txtcontactname.TabIndex = 35;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox5.Controls.Add(this.rbseparated);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.rbdivorced);
            this.groupBox5.Controls.Add(this.rbsingle);
            this.groupBox5.Controls.Add(this.rbmarried);
            this.groupBox5.Controls.Add(this.txtstatus);
            this.groupBox5.Location = new System.Drawing.Point(181, 428);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(200, 168);
            this.groupBox5.TabIndex = 39;
            this.groupBox5.TabStop = false;
            // 
            // txtstatus
            // 
            this.txtstatus.AutoSize = true;
            this.txtstatus.Location = new System.Drawing.Point(42, 114);
            this.txtstatus.Name = "txtstatus";
            this.txtstatus.Size = new System.Drawing.Size(79, 19);
            this.txtstatus.TabIndex = 41;
            this.txtstatus.Text = "Separated";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox6.Controls.Add(this.rbafrican);
            this.groupBox6.Controls.Add(this.rbasian);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.rbhispanic);
            this.groupBox6.Controls.Add(this.rbalaskan);
            this.groupBox6.Controls.Add(this.rbwhite);
            this.groupBox6.Controls.Add(this.txtethnicity);
            this.groupBox6.Location = new System.Drawing.Point(395, 427);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(295, 170);
            this.groupBox6.TabIndex = 40;
            this.groupBox6.TabStop = false;
            // 
            // txtethnicity
            // 
            this.txtethnicity.AutoSize = true;
            this.txtethnicity.Location = new System.Drawing.Point(37, 139);
            this.txtethnicity.Name = "txtethnicity";
            this.txtethnicity.Size = new System.Drawing.Size(238, 19);
            this.txtethnicity.TabIndex = 42;
            this.txtethnicity.Text = "Alaskan Native / Native American";
            // 
            // btnSaveEmpInfo
            // 
            this.btnSaveEmpInfo.Location = new System.Drawing.Point(1117, 569);
            this.btnSaveEmpInfo.Name = "btnSaveEmpInfo";
            this.btnSaveEmpInfo.Size = new System.Drawing.Size(75, 30);
            this.btnSaveEmpInfo.TabIndex = 41;
            this.btnSaveEmpInfo.Text = "SAVE";
            this.btnSaveEmpInfo.UseVisualStyleBackColor = true;
            this.btnSaveEmpInfo.Click += new System.EventHandler(this.btnSaveEmpInfo_Click);
            // 
            // txtstartdatesuffix
            // 
            this.txtstartdatesuffix.Enabled = false;
            this.txtstartdatesuffix.Location = new System.Drawing.Point(149, 8);
            this.txtstartdatesuffix.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtstartdatesuffix.Name = "txtstartdatesuffix";
            this.txtstartdatesuffix.Size = new System.Drawing.Size(72, 27);
            this.txtstartdatesuffix.TabIndex = 42;
            this.txtstartdatesuffix.Text = "DN10515";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(770, 561);
            this.label25.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(262, 19);
            this.label25.TabIndex = 43;
            this.label25.Text = "*Please type N/A if NOT APPLICABLE.";
            // 
            // AddEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(1218, 613);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.txtstartdatesuffix);
            this.Controls.Add(this.btnSaveEmpInfo);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.txtempid);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "AddEmployee";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EmployeeInfo";
            this.Load += new System.EventHandler(this.EmployeeInfo_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbempbirthmonth;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtempmidname;
        private System.Windows.Forms.TextBox txtemplastname;
        private System.Windows.Forms.TextBox txtempfirstname;
        private System.Windows.Forms.TextBox txtpagibigno;
        private System.Windows.Forms.TextBox txtphno;
        private System.Windows.Forms.TextBox txttin;
        private System.Windows.Forms.TextBox txtssno;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtfbacct;
        private System.Windows.Forms.TextBox txtskypename;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtemailaddress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txthomeaddress;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtcontactno;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbempbirthday;
        private System.Windows.Forms.ComboBox cmbempbirthyear;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbfemale;
        private System.Windows.Forms.RadioButton rbsingle;
        private System.Windows.Forms.RadioButton rbalaskan;
        private System.Windows.Forms.RadioButton rbwhite;
        private System.Windows.Forms.RadioButton rbseparated;
        private System.Windows.Forms.RadioButton rbhispanic;
        private System.Windows.Forms.RadioButton rbmarried;
        private System.Windows.Forms.RadioButton rbdivorced;
        private System.Windows.Forms.RadioButton rbafrican;
        private System.Windows.Forms.RadioButton rbasian;
        private System.Windows.Forms.RadioButton rbmale;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtempid;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtstartdate;
        private System.Windows.Forms.TextBox txtworklocation;
        private System.Windows.Forms.TextBox txtphone;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtposition;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtrelationship;
        private System.Windows.Forms.TextBox txtalterphone;
        private System.Windows.Forms.TextBox txtpriphone;
        private System.Windows.Forms.TextBox txtcontactaddress;
        private System.Windows.Forms.TextBox txtcontactname;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label txtgender;
        private System.Windows.Forms.Label txtstatus;
        private System.Windows.Forms.Label txtethnicity;
        private System.Windows.Forms.Button btnSaveEmpInfo;
        private System.Windows.Forms.TextBox txtstartdatesuffix;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.DateTimePicker dtstartdate;
    }
}