﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invoice
{
    public class InvoiceEntity
    {
        [BsonId]
        public string invoiceNumber { get; set; }
        public string clientId { get; set; }
        public string clientAcctName { get; set; }
        public string clientStreet { get; set; }
        public string clientCity { get; set; }
        public string clientCountry { get; set; }
        public string clientRep { get; set; }
        public DateTime datenow { get; set; }
        public DateTime datedue { get; set; }
        public string description { get; set; }
        public string note { get; set; }
        public double amount { get; set; }
        public string vat { get; set; }
        public double totalamt { get; set; }

    }
}
