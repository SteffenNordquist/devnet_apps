﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Diagnostics;

namespace Invoice
{
    public class GeneratePDF
    {
        private static BaseFont font = BaseFont.CreateFont("c:\\windows\\fonts\\calibrib.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        public static void GeneratePdf(string pdfPathForSaving, string filename , string clientacctno,string clientname, string clientstreet, string clientcity, string clientcountry, string rep, string invoicenum, string date, string datedue,string desc, string note, string amount, string vat, string totalamt)
        {
           FileInfo pdfFile = new FileInfo(pdfPathForSaving + filename);
           using (System.IO.FileStream fileStream = new FileStream(pdfFile.FullName, FileMode.Create))
           {
               Document document = new Document(PageSize.A4, 25, 25, 30, 1);
               PdfWriter writer = PdfWriter.GetInstance(document, fileStream);
               document.Open();

               //add logo
               iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance("logo.jpg");
               logo.ScalePercent(10f);
               logo.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
               //logo.SetAbsolutePosition(180f, 200f);
               document.Add(logo);

               PdfContentByte cb = writer.DirectContent;

               cb.BeginText();

               //Header
               WriteHeader(cb, "2nd Floor, Insular Bldg.", 300, 740, font, 12);
               WriteHeader(cb, "Don Anselmo Bernad Avenue, Ozamiz City", 300, 725, font, 12);
               WriteHeader(cb, "Misamis Occidental, Philippines 7200", 300, 710, font, 12);
               WriteHeader(cb, "TIN: 462 943 486 000 ", 300, 695, font, 12);

               //Client Info
               WriteClientInfo(cb, clientname, 60, 650, font, 12);
               WriteClientInfo(cb, clientstreet, 60, 635, font, 12);
               WriteClientInfo(cb, clientcity, 60, 620, font, 12);
               WriteClientInfo(cb, clientcountry, 60, 605, font, 12);
               WriteClientInfo(cb, "Attention: " + rep, 60, 575, font, 12);

               //Invoice
               WriteInvoiceInfo(cb, "INVOICE", 370, 650, font, 16);
               WriteInvoiceInfo(cb, "Invoice No    :   " + invoicenum, 370, 620, font, 12);
               WriteInvoiceInfo(cb, "Invoice Date :   " + date, 370, 605, font, 12);
               WriteInvoiceInfo(cb, "Due Date       :   " + datedue, 370, 590, font, 12);
               WriteInvoiceInfo(cb, "Client Acct    :   " + clientacctno, 370, 575, font, 12);

               
               //Table

               PdfPTable table = new PdfPTable(2);
               table.TotalWidth = 480f;
               table.LockedWidth = true;
               table.SetTotalWidth(new float[] { 360f, 110f });

               PdfPCell cell1 = new PdfPCell();
               cell1.Colspan = 1;
               cell1.AddElement(new Paragraph("Description", new iTextSharp.text.Font(iTextSharp.text.FontFactory.GetFont("calibri", 12, iTextSharp.text.Font.BOLD))));
               cell1.HorizontalAlignment = Element.ALIGN_CENTER;
               
               PdfPCell cell2 = new PdfPCell();
               cell2.AddElement(new Paragraph("Amount (PHP)", new iTextSharp.text.Font(iTextSharp.text.FontFactory.GetFont("calibri", 12, iTextSharp.text.Font.BOLD))));              

               PdfPCell cell3 = new PdfPCell();
               cell3.AddElement(new Paragraph(desc, new iTextSharp.text.Font(iTextSharp.text.FontFactory.GetFont("calibri", 12, iTextSharp.text.Font.BOLDITALIC))));
               cell3.FixedHeight = 300.0f;
               

               PdfPCell cell4 = new PdfPCell();
               cell4.AddElement(new Paragraph(amount, new iTextSharp.text.Font(iTextSharp.text.FontFactory.GetFont("calibri", 12, iTextSharp.text.Font.BOLDITALIC))));
               cell4.FixedHeight = 300.0f;

               PdfPCell cell5 = new PdfPCell();
               cell5.AddElement(new Paragraph("Total Amount:"));
               cell5.FixedHeight = 75.0f;

               PdfPCell cell6 = new PdfPCell();
               cell6.AddElement(new Paragraph(amount, new iTextSharp.text.Font(iTextSharp.text.FontFactory.GetFont("calibri", 12, iTextSharp.text.Font.BOLDITALIC))));
               cell6.FixedHeight = 75.0f;

               PdfPCell cell7= new PdfPCell();
               cell5.AddElement(new Paragraph("VAT:"));
               cell5.FixedHeight = 75.0f;

               PdfPCell cell8 = new PdfPCell();
               cell6.AddElement(new Paragraph(vat, new iTextSharp.text.Font(iTextSharp.text.FontFactory.GetFont("calibri", 12, iTextSharp.text.Font.BOLDITALIC))));
               cell6.FixedHeight = 75.0f;

               PdfPCell cell9 = new PdfPCell();
               cell5.AddElement(new Paragraph("Total Invoice Amount:"));
               cell5.FixedHeight = 75.0f;

               PdfPCell cell10 = new PdfPCell();
               cell6.AddElement(new Paragraph(totalamt, new iTextSharp.text.Font(iTextSharp.text.FontFactory.GetFont("calibri", 12, iTextSharp.text.Font.BOLDITALIC))));
               cell6.FixedHeight = 75.0f;

               table.AddCell(cell1);
               table.AddCell(cell2);
               table.AddCell(cell3);
               table.AddCell(cell4);
               table.AddCell(cell5);
               table.AddCell(cell6);
               table.AddCell(cell7);
               table.AddCell(cell8);
               table.AddCell(cell9);
               table.AddCell(cell10);

               table.WriteSelectedRows(0, -1, document.PageSize.GetLeft(60), document.PageSize.GetTop(290), writer.DirectContent);

               //Note
               WriteInvoiceInfo(cb, "*Note: " + note, 60, 140, font, 12);

               //Signature
               WriteSignature(cb, "Received By    : ", 60, 100, font, 12);
               WriteSignature(cb, "Printed Name : _____________________", 60, 85, font, 12);
               WriteSignature(cb, "Date                  : _____________________", 60, 70, font, 12);
               WriteSignature(cb, "Signature         : _____________________", 60, 55, font, 12);

               //Footer
               WriteFooter(cb, "________________________________", 450, 85, font, 10);
               WriteFooter(cb, "This is a system-generated document.", 450, 75, font, 8);
               WriteFooter(cb, "No signature is required, if issued without alteration.", 450, 65, font, 8);
               cb.EndText();
               document.Close(); 
               writer.Close();
               fileStream.Close();

               Process.Start(pdfFile.FullName);
           }

       }
        private static void WriteHeader(PdfContentByte cb, string Text, int X, int Y, BaseFont font, int Size)
        {
            cb.SetFontAndSize(font, Size);
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, Text, X, Y, 0);
        }
        private static void WriteClientInfo(PdfContentByte cb, string Text, int X, int Y, BaseFont font, int Size)
        {
            cb.SetFontAndSize(font, Size);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Text, X, Y, 0);
        }
        private static void WriteInvoiceInfo(PdfContentByte cb, string Text, int X, int Y, BaseFont font, int Size)
        {
            cb.SetFontAndSize(font, Size);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Text, X, Y, 0);
        }

        private static void WriteNote(PdfContentByte cb, string Text, int X, int Y, BaseFont font, int Size)
        {
            cb.SetFontAndSize(font, Size);
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, Text, X, Y, 0);
        }

        private static void WriteSignature(PdfContentByte cb, string Text, int X, int Y, BaseFont font, int Size)
        {
            cb.SetFontAndSize(font, Size);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Text, X, Y, 0);
        }

        private static void WriteFooter(PdfContentByte cb, string Text, int X, int Y, BaseFont font, int Size)
        {
            cb.SetFontAndSize(font, Size);
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, Text, X, Y, 0);
        }
           
    }
}
