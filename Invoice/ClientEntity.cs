﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Invoice
{
    public class ClientEntity
    {
        [BsonId]
        public string clientId { get; set; }
        public string clientAcctName { get; set; }
        public string clientStreet { get; set; }
        public string clientCity { get; set; }
        public string clientCountry { get; set; }
        public string clientRep { get; set; }
    }
}
