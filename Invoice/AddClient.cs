﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Invoice
{
    public partial class AddClient : Form
    {
        public AddClient()
        {
            InitializeComponent();
        }

        //private static void MongoCollection<ClientEntity> collection;

        public static MongoCollection<ClientEntity> Client = new MongoClient(@"mongodb://client144:client144devnetworks@144.76.166.207/DevNet").GetServer().GetDatabase("DevNet").GetCollection<ClientEntity>("Clients");


        private void btnAddClient_Click(object sender, EventArgs e)
        {
            try
            {
                ClientEntity client = new ClientEntity
                {
                    clientId = txtclientSuffix.Text.Substring(0, 4) + txtclientidnum.Text,
                    clientAcctName = txtAcctName.Text,
                    clientStreet = txtstreet.Text,
                    clientCity = txtcity.Text,
                    clientCountry = txtcountry.Text,
                    clientRep = txtcompanyrep.Text
                };
                Client.Insert(client);

                MessageBox.Show("Client Successfully Added!");
            }
            catch
            {
                MessageBox.Show("Client Already Added!");
            }
        }




        
        private void AddClient_Load(object sender, EventArgs e)
        {
            Random r = new Random();
            txtclientidnum.Text = ("-" + r.Next(1000, 9999).ToString() + "-" + r.Next(1000, 9999).ToString());
            

        }


        private void txtAcctName_TextChanged(object sender, EventArgs e)
        {
            txtclientSuffix.Text = txtAcctName.Text.ToUpper();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
